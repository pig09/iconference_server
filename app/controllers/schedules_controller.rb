class SchedulesController < ApplicationController
  # GET /schedules
  # GET /schedules.json
  def index
    if( params[:user_id] && params[:conference_id])
      @attendee = Attendee.where('conference_id = ? AND user_id = ?', params[:conference_id], params[:user_id]).first
      if (@attendee)
        @schedule = Schedule.find(@attendee.schedule_id)
        render 'schedules/index.json.jbuilder'
      else
        head :not_found
      end
  else
      @schedules = Schedule.all

      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @schedules }
      end

    end

  end

  # GET /schedules/1
  # GET /schedules/1.json
  def show
    @schedule = Schedule.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render 'schedules/show.json.jbuilder' }
    end
  end

  # GET /schedules/new
  # GET /schedules/new.json
  def new
    @schedule = Schedule.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @schedule }
    end
  end

  # GET /schedules/1/edit
  def edit
    @schedule = Schedule.find(params[:id])
  end

  # POST /schedules
  # POST /schedules.json
  def create
    @schedule = Schedule.new(params[:schedule])

    respond_to do |format|
      if @schedule.save
        format.html { redirect_to @schedule, notice: 'Schedule was successfully created.' }
        format.json { render json: @schedule, status: :created, location: @schedule }
      else
        format.html { render action: "new" }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /schedules/1
  # PUT /schedules/1.json
  def update
    if(params[:results] && params[:user_id] && params[:conference_id])
      attendee = Attendee.where('conference_id = ? AND user_id = ?', params[:conference_id], params[:user_id]).first
      schedule = Schedule.find(attendee.schedule_id)
      schedule.events.delete_all
      schedule.sessions.delete_all
      results = params[:results]
      results.each do |result|
        if (result[:type] == 'session' )
          schedule.sessions << Session.find(result[:id])
        elsif (result[:type] == 'event' )
          schedule.events << Event.find(result[:id])
        end
      end
      head :ok
    else
      @schedule = Schedule.find(params[:id])


      respond_to do |format|
        if @schedule.update_attributes(params[:schedule])
          format.html { redirect_to @schedule, notice: 'Schedule was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @schedule.errors, status: :unprocessable_entity }
        end
      end
    end
  end


  def add
    if(params[:results] && params[:user_id] && params[:conference_id])
      attendee = Attendee.where('conference_id = ? AND user_id = ?', params[:conference_id], params[:user_id]).first
      if( attendee )
        schedule = Schedule.find(attendee.schedule_id)
        results = params[:results]
        results.each do |result|
          if (result[:type] == 'session' && !schedule.sessions.exists?(result[:id]))
            schedule.sessions << Session.find(result[:id])
          elsif (result[:type] == 'event' && !schedule.events.exists?(result[:id]))
            schedule.events << Event.find(result[:id])
          end
          head :ok
        end
      else
        head :bad_request
      end
    else
      head :bad_request
    end
  end


  def remove
    if(params[:results] && params[:user_id] && params[:conference_id])
      attendee = Attendee.where('conference_id = ? AND user_id = ?', params[:conference_id], params[:user_id]).first
      if( attendee )
        schedule = Schedule.find(attendee.schedule_id)
        results = params[:results]
        results.each do |result|
          if (result[:type] == 'session' && schedule.sessions.exists?(result[:id]))
            schedule.sessions.delete(Session.find(result[:id]))
          elsif (result[:type] == 'event' && schedule.events.exists?(result[:id]))
            schedule.events.delete(Event.find(result[:id]))
          end
          head :ok
          end
        else
          head :bad_request
        end
      else
        head :bad_request
      end
  end

  # DELETE /schedules/1
  # DELETE /schedules/1.json
  def destroy
    @schedule = Schedule.find(params[:id])
    @schedule.destroy

    respond_to do |format|
      format.html { redirect_to schedules_url }
      format.json { head :no_content }
    end
  end
end
