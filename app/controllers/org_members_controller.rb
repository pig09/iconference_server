class OrgMembersController < ApplicationController
  # GET /org_members
  # GET /org_members.json
  def index
    @org_members = OrgMember.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @org_members }
    end
  end

  # GET /org_members/1
  # GET /org_members/1.json
  def show
    @org_member = OrgMember.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @org_member }
    end
  end

  # GET /org_members/new
  # GET /org_members/new.json
  def new
    @org_member = OrgMember.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @org_member }
    end
  end

  # GET /org_members/1/edit
  def edit
    @org_member = OrgMember.find(params[:id])
  end

  # POST /org_members
  # POST /org_members.json
  def create
    @org_member = OrgMember.new(params[:org_member])

    respond_to do |format|
      if @org_member.save
        format.html { redirect_to @org_member, notice: 'Org member was successfully created.' }
        format.json { render json: @org_member, status: :created, location: @org_member }
      else
        format.html { render action: "new" }
        format.json { render json: @org_member.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /org_members/1
  # PUT /org_members/1.json
  def update
    @org_member = OrgMember.find(params[:id])

    respond_to do |format|
      if @org_member.update_attributes(params[:org_member])
        format.html { redirect_to @org_member, notice: 'Org member was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @org_member.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /org_members/1
  # DELETE /org_members/1.json
  def destroy
    @org_member = OrgMember.find(params[:id])
    @org_member.destroy

    respond_to do |format|
      format.html { redirect_to org_members_url }
      format.json { head :no_content }
    end
  end
end
