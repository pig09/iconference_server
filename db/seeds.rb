# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' ', { name: 'Copenhagen' '])
#   Mayor.create(name: 'Emanuel', city: cities.first)

print "\nSeeding the database...\n"


# Org Members
print "-- Creating Org Members...\n"
OrgMember.delete_all
OrgMember.create(:first_name=> 'Bruno', :last_name => 'Candeias', :email => 'bruno.gcandeias@live.com', :password => 'secret')


#Institution
print "-- Creating Institutions...\n"
Institution.delete_all
Institution.create(:name=> 'Faculdade de Ciências da Universidade de Lisboa', :country =>'Portugal', :website =>'www.fcul.pt')
Institution.create(:name=> 'Faculdade de Ciências e Tecnologia da Universidade de Coimbra', :country =>'Portugal', :website =>'www.fct.uc.pt')
Institution.create(:name=> 'Faculdade de Ciências e Tecnologia da Universidade Nova de Lisboa', :country =>'Portugal', :website =>'www.fct.unl.pt')
Institution.create(:name=> 'IBM', :country =>'Portugal', :website =>'www.ibm.com')
Institution.create(:name=> 'INESC-ID', :country =>'Portugal', :website =>'www.inesc-id.pt')
Institution.create(:name=> 'Instituto Superior Técnico da Universidade Técnica de Lisboa', :country =>'Portugal', :website =>'www.ist.utl.pt')
Institution.create(:name=> 'Instituto de Telecomunicações Universidade da Beira Interior', :country =>'Portugal', :website =>'www.ubi.pt')


#Conferences
print "-- Creating Conferences...\n"
Conference.delete_all

Conference.create(:avatar => '/uploads/conference/avatar/1/inforum_72ppi-01.png', :name => 'INForum 2012',
                  :local =>'Almada, Portugal',
                  :description =>'A Informática é uma área de Investigação e Desenvolvimento consolidada em Portugal, suportada numa vasta rede de centros de investigação reconhecidos institucionalmente,
assim como na oferta de cursos de licenciatura e pós-graduação por praticamente todas as instituições portuguesas de Ensino Superior. O 4º INForum será organizado pela Faculdade de Ciências e Tecnologia da Universidade Nova de Lisboa e decorrerá nos dias 6 e 7 de Setembro de 2012 no Campus da Caparica.', :conf_date =>DateTime.strptime('06/09/2012
12:00', '%d/%m/%Y %H:%M'), :hashtag => 'inforum')

Conference.create(:avatar => '/uploads/conference/avatar/2/eurosys-02.png', :name => 'EuroSys 2013', :local =>'Prague, Czech Republic',
                  :description =>'The EuroSys conference series brings together professionals from academia and industry.
It has a strong focus on systems research and development: operating systems, data base systems, real-time systems and middleware for networked, distributed, parallel, or embedded computing systems. EuroSys has become a premier forum for discussing various issues of systems software research and development, including implications related to hardware and applications.
EuroSys 2013 will follow the pattern established by the previous EuroSys conferences, by seeking papers on all aspects of computer
systems.', :conf_date =>DateTime.strptime('14/04/2013 08:00', '%d/%m/%Y %H:%M'), :hashtag => 'eurosys ')

Conference.create(:name => 'Test 2013', :local =>'Somewhere',
                  :description =>'Test', :conf_date =>DateTime.strptime('14/04/2013 08:00', '%d/%m/%Y %H:%M'))


print "-- Seeding INForum...\n"

# Users
print "-- Creating Users...\n"
User.delete_all
User.create(:firstName => 'Demo', :lastName => 'Confy', :email => 'confy@demo.com',  :password => '123456')
#User.create(:firstName => 'Bruno', :lastName => 'Candeias', :email => 'bruno.gcandeias@live.com',  :password => 'secret')
#User.create(:firstName => 'Joao', :lastName => 'Rosa', :email => 'jrosa@mail.com', :password => 'password')
User.create(:firstName => 'Bruno', :lastName => 'Candeias', :email => 'bcandeias@mail.com', :password => 'password')
User.create(:firstName => 'Daniel', :lastName => 'Dionisio', :email => 'ddionisio@mail.com', :password => 'password')
User.create(:firstName => 'Ricardo', :lastName => 'Mendes', :email =>'rmendes@lasige.di.fc.ul.pt',:institution_id =>1, :password =>'password')
User.create(:firstName => 'Alysson', :lastName => 'Bessani', :email =>'bessani@di.fc.ul.pt', :photo =>'http://www.di.fc.ul.pt/~bessani/me.jpg', :institution_id =>1, :password =>'password')
User.create(:firstName => 'Tiago', :lastName => 'Oliveira', :email =>'toliveira@lasige.di.fc.ul.pt',:institution_id =>1, :password =>'password')
User.create(:firstName => 'Marcelo', :lastName => 'Pasin', :email =>'pasin@di.fc.ul.pt',:institution_id =>1, :password =>'password')
User.create(:firstName => 'Serhiy', :lastName => 'Boychenko', :email =>'serhiy@student.dei.uc.pt', :institution_id =>2, :password =>'password')
User.create(:firstName => 'Filipe', :lastName => 'Araujo', :email =>'filipus@dei.uc.pt', :institution_id =>2, :password =>'password')
User.create(:firstName => 'Diogo', :lastName => 'Sousa', :email =>'dm.sousa@campus.fct.unl.pt', :photo =>'http://sites.fct.unl.pt/sites/default/files/imagecache/vsite_design_portrait_logo/synergy-vm/files/diogo_sousa.jpg', :institution_id =>3, :password =>'password')
User.create(:firstName => 'João', :lastName => 'Lourenço', :email =>'joao.lourenco@fct.unl.pt', :photo =>'http://citi.di.fct.unl.pt/member/members_photos/joao.lourenco.jpg', :institution_id =>3, :password =>'password')
User.create(:firstName => 'Eitan', :lastName => 'Farchi', :email =>'farchi@il.ibm.com', :photo =>'http://researcher.watson.ibm.com/researcher/photos/1013.jpg', :institution_id =>4, :password =>'password')
User.create(:firstName => 'Itai', :lastName => 'Segall', :email =>'itais@il.ibm.com', :photo =>'http://researcher.watson.ibm.com/researcher/photos/1248.jpg', :institution_id =>4, :password =>'password')
User.create(:firstName => 'Navaneeth', :lastName => 'Rameshan', :email =>'navaneeth.rameshan@gmail.com', :institution_id =>5, :password =>'password')
User.create(:firstName => 'Luis', :lastName => 'Veiga', :email =>'luis.veiga@inesc-id.pt', :photo =>'http://www.gsd.inesc-id.pt/~lveiga/images/lveiga-2008.jpg', :institution_id =>5, :password =>'password')
User.create(:firstName => 'Valter', :lastName => 'Balegas', :email =>'v.sousa@campus.fct.unl.pt',:institution_id =>3, :password =>'password')
User.create(:firstName => 'Nuno', :lastName => 'Preguiça', :email =>'nuno.preguica@di.fct.unl.pt', :photo =>'http://citi.di.fct.unl.pt/member/members_photos/nmp.jpg', :institution_id =>3, :password =>'password')
User.create(:firstName => 'Miguel', :lastName => 'Branco', :email =>'miguel.branco@ist.unl.pt', :institution_id =>6, :password =>'password')
#User.create(:firstName => 'João', :lastName => 'Leitão', :email =>'jleitão@gsd.inesc-id.pt', :photo =>'http://www.gsd.inesc-id.pt/~jleitao/graph/IMG_2471.jpg', :institution_id =>5, :password =>'password')
User.create(:firstName => 'João', :lastName => 'Leitão', :email =>'jleitão@gsd.inesc-id.pt', :institution_id =>5, :password =>'password')
#User.create(:firstName => 'Luís', :lastName => 'Rodrigues', :email =>'ler@ist.utl.pt', :photo =>'http://www.gsd.inesc-id.pt/~ler/misc/minema.JPG', :institution_id =>6, :password =>'password')
User.create(:firstName => 'Luís', :lastName => 'Rodrigues', :email =>'ler@ist.utl.pt', :institution_id =>6, :password =>'password')
User.create(:firstName => 'João', :lastName => 'Silva', :email =>'joao.m.silva@ist.utl.pt', :photo =>'https://fenix.ist.utl.pt/publico/retrievePersonalPhoto.do?method=retrievePhotographOnPublicSpace&personId=2306397439574&contentContextPath_PATH=/homepage/ist12570/apresentacao&_request_checksum_=23085ac889153649da84e5e9c16377a3346e4df6', :institution_id =>5, :password =>'password')
User.create(:firstName => 'Ricardo', :lastName => 'Marques', :email =>'',  :institution_id =>3, :password =>'password')
User.create(:firstName => 'Hervé', :lastName => 'Paulino', :email =>'herve.paulino@fct.unl.pt', :photo =>'http://citi.di.fct.unl.pt/member/members_photos/herve.jpg', :institution_id =>3, :password =>'password')
User.create(:firstName => 'Pedro', :lastName => 'Medeiros', :email =>'pdm@fct.unl.pt', :photo =>'http://asc.di.fct.unl.pt/~pm/pm2.jpg', :institution_id =>3, :password =>'password')
User.create(:firstName => 'Sérgio', :lastName => 'Almeida', :email =>'sergiogarrau@gsd.inesc-id.pt', :institution_id =>5, :password =>'password')
User.create(:firstName => 'João', :lastName => 'Félix', :email =>'', :institution_id =>1, :password =>'password')
User.create(:firstName => 'Miguel', :lastName => 'Correia', :email =>'miguel.p.correia@ist.utl.pt', :photo =>'http://www.gsd.inesc-id.pt/~mpc/images/mpc2013-04-11n.jpg', :institution_id =>5, :password =>'password')
User.create(:firstName => 'Ricardo', :lastName => 'Caldeira', :email =>'ricardo.caldeira@ist.utl.pt', :institution_id =>5, :password =>'password')
User.create(:firstName => 'Ricardo', :lastName => 'Dias', :email =>'ricardo.dias@campus.fct.unl.pt', :photo =>'http://citi.di.fct.unl.pt/member/members_photos/rjd15372.jpg', :institution_id =>3, :password =>'password')
User.create(:firstName => 'Tiago', :lastName => 'Vale', :email =>'t.vale@campus.fct.unl.pt', :photo =>'http://citi.di.fct.unl.pt/member/members_photos/t.vale.jpg', :institution_id =>3, :password =>'password')
User.create(:firstName => 'Manuel', :lastName => 'Cajada', :email =>'cajadas@gmail.com',:institution_id =>5, :password =>'password')
User.create(:firstName => 'Paulo', :lastName => 'Ferreira', :email =>'paulo.ferreira@gsd.inesc-id.pt', :photo =>'http://homepages.gsd.inesc-id.pt/~pjpf/paulo-ferreira.jpg', :institution_id =>5, :password =>'password')
User.create(:firstName => 'João', :lastName => 'Pereira', :email =>'joaodrp@it.ubi.pt', :institution_id =>7, :password =>'password')
User.create(:firstName => 'Paula', :lastName => 'Prata', :email =>'pprata@di.ubi.pt', :institution_id =>7, :password =>'password')
User.create(:firstName => 'Nuno', :lastName => 'Delgado', :email =>'', :institution_id =>3, :password =>'password')



#Schedules
print "-- Creating Schedules...\n"
Schedule.delete_all
Schedule.create()
Schedule.create()
Schedule.create()
Schedule.create()

#Attendees
print "-- Creating Attendees...\n"
Attendee.delete_all
Attendee.create(:checkin => true, :conference_id => '1', :user_id =>'1', :schedule_id => '1')
Attendee.create(:checkin => true, :conference_id => '2', :user_id =>'1', :schedule_id => '2')
#Attendee.create(:checkin => true, :conference_id => '3', :user_id =>'1', :schedule_id => '3')
Attendee.create(:checkin => true, :conference_id => '1', :user_id =>'2')
Attendee.create(:checkin => true, :conference_id => '2', :user_id =>'2')
Attendee.create(:checkin => true, :conference_id => '3', :user_id =>'2')
Attendee.create(:checkin => true, :conference_id => '1', :user_id =>'3')
Attendee.create(:checkin => true, :conference_id => '2', :user_id =>'3')
Attendee.create(:checkin => true, :conference_id => '3', :user_id =>'3')

# Rooms
print "-- Creating Rooms...\n"
Room.delete_all
Room.create(:name => 'Room 1', :capacity => '50')
Room.create(:name => 'Room 2', :capacity => '50')
Room.create(:name => 'Room 3', :capacity => '50')

#Sessions
print "-- Creating Sessions...\n"
Session.delete_all
Session.create(:chair => 'Jonh Smith', :name => 'Computação Paralela, Distribuída e de Larga Escala 1',
               :conference_id =>'1', :room_id => '1')
Session.create(:chair => 'Jonh Smith', :name => 'Computação Paralela, Distribuída e de Larga Escala 2',
               :conference_id =>'1', :room_id => '1')
Session.create(:chair => 'Jonh Smith', :name => 'Computação Paralela, Distribuída e de Larga Escala 3',
               :conference_id =>'1', :room_id => '1')
Session.create(:chair => 'Jonh Smith', :name => 'Computação Paralela, Distribuída e de Larga Escala 4',
               :conference_id =>'1', :room_id => '1')


#Presentations
print "-- Creating Presentations...\n"
Presentation.delete_all
Presentation.create(:start_at =>DateTime.strptime('06/09/2012 14:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('06/09/2012 14:30', '%d/%m/%Y %H:%M'),
                    :title =>'C2FS: um Sistema de Ficheiros Seguro e Fiável para Cloud-of-clouds - Ricardo Mendes,
Tiago Oliveira, Alysson Bessani, Marcelo Pasin', :session_id =>'1', :article => 'http://inforum.org.pt/INForum2012/docs/20120256.pdf')
Presentation.create(:start_at => DateTime.strptime('06/09/2012 14:30', '%d/%m/%Y %H:%M'),
                    :end_at =>DateTime.strptime('06/09/2012 15:00', '%d/%m/%Y %H:%M'),
                    :title =>'Benchmarking the EDGI Infrastructure-Serhiy Boychenko, Filipe Araújo',
                    :session_id =>'1', :article =>  'http://inforum.org.pt/INForum2012/docs/20120268.pdf')
Presentation.create(:start_at => DateTime.strptime('06/09/2012 15:00', '%d/%m/%Y %H:%M'),
                    :end_at =>DateTime.strptime('06/09/2012 15:30', '%d/%m/%Y %H:%M'),
                    :title =>'Aplicação do Fecho de Programas na Deteção de Anomalias de Concorrência-Diogo G. Sousa,
 João Lourenço, Eitan Farchi, Itai Segall', :session_id =>'1', :article =>  'http://inforum.org.pt/INForum2012/docs/20120184.pdf')
Presentation.create(:start_at => DateTime.strptime('06/09/2012 15:30', '%d/%m/%Y %H:%M'),
                    :end_at =>DateTime.strptime('06/09/2012 16:00', '%d/%m/%Y %H:%M'),
                    :title =>'RATS --  Resource Aware Thread Scheduling for JVM-level Clustering - Navaneeth Rameshan,
 Luís Veiga', :session_id =>'1', :article =>  'http://inforum.org.pt/INForum2012/docs/20120280.pdf')

Presentation.create(:start_at =>DateTime.strptime('06/09/2012 16:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('06/09/2012 16:50', '%d/%m/%Y %H:%M'),
                    :title =>'SwiftCloud: replicação sem coordenação-Valter Balegas, Nuno Preguiça',
                    :session_id =>'2', :article => 'http://inforum.org.pt/INForum2012/docs/20120220.pdf')

Presentation.create(:start_at => DateTime.strptime('06/09/2012 16:50', '%d/%m/%Y %H:%M'),
                    :end_at =>DateTime.strptime('06/09/2012 17:10', '%d/%m/%Y %H:%M'),
                    :title =>'PEC: Protocolo Epidémico para Centros de dados-Miguel Branco', :session_id =>'2', :article =>  'http://inforum.org.pt/INForum2012/docs/20120196.pdf')

#Presentation.create(:start_at => DateTime.strptime('06/09/2012 16:50', '%d/%m/%Y %H:%M'),
#                    :end_at =>DateTime.strptime('06/09/2012 17:10', '%d/%m/%Y %H:%M'),
#                    :title =>'PEC: Protocolo Epidémico para Centros de dados-Miguel Branco, João Leitão, Luís Rodrigues', :session_id =>'2', :article =>  'http://inforum.org.pt/INForum2012/docs/20120196.pdf')

Presentation.create(:start_at => DateTime.strptime('06/09/2012 17:10', '%d/%m/%Y %H:%M'),
                    :end_at =>DateTime.strptime('06/09/2012 17:40', '%d/%m/%Y %H:%M'),
                    :title =>'Reprodução Probabilística de Execuções na JVM em Multi-processadores-João Silva,
Luís Veiga', :session_id =>'2', :article =>  'http://inforum.org.pt/INForum2012/docs/20120300.pdf')
Presentation.create(:start_at => DateTime.strptime('06/09/2012 17:40', '%d/%m/%Y %H:%M'),
                    :end_at =>DateTime.strptime('06/09/2012 18:00', '%d/%m/%Y %H:%M'),
                    :title =>'Desenho e Implementação de uma Biblioteca de Padrões Algorítmicos para GPGPU-Ricardo
Marques, Hervé Paulino, Pedro Medeiros', :session_id =>'2', :article =>  'http://inforum.org.pt/INForum2012/docs/20120292.pdf')

Presentation.create(:start_at =>DateTime.strptime('07/09/2012 10:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('07/09/2012 11:00', '%d/%m/%Y %H:%M'),
                    :title =>'ChainReaction: uma Variante de Replicação em Cadeia com Coerência Causal+ - Sérgio Almeida', :session_id =>'3', :article => 'http://inforum.org.pt/INForum2012/docs/20120208.pdf')
Presentation.create(:start_at => DateTime.strptime('07/09/2012 11:00', '%d/%m/%Y %H:%M'),
                    :end_at =>DateTime.strptime('07/09/2012 11:30', '%d/%m/%Y %H:%M'),
                    :title =>'Gestão de Estado Eficiente no Serviço de Coordenação DDS-João Félix, Alysson Bessani,
Miguel Correia', :session_id =>'3', :article =>  'http://inforum.org.pt/INForum2012/docs/20120232.pdf')

Presentation.create(:start_at => DateTime.strptime('07/09/2012 11:30', '%d/%m/%Y %H:%M'),
                    :end_at =>DateTime.strptime('07/09/2012 12:00', '%d/%m/%Y %H:%M'),
                    :title =>'FaceID-Cloud --  Face Identification Leveraging Utility and Cloud Computing - Ricardo
Caldeira, Luís Veiga', :session_id =>'3', :article =>  'http://inforum.org.pt/INForum2012/docs/20120244.pdf')

Presentation.create(:start_at =>DateTime.strptime('07/09/2012 16:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('07/09/2012 16:50', '%d/%m/%Y %H:%M'),
                    :title =>'Uma Infraestrutura para Suporte de Memória Transacional Distribuída-Tiago Vale,
Ricardo Dias, João Lourenço', :session_id =>'4', :article => 'http://inforum.org.pt/INForum2012/docs/20120172.pdf')
Presentation.create(:start_at => DateTime.strptime('07/09/2012 16:50', '%d/%m/%Y %H:%M'),
                    :end_at =>DateTime.strptime('07/09/2012 17:10', '%d/%m/%Y %H:%M'),
                    :title =>'VFC-RTS: Vector-Field Consistency for Real-Time-Strategy Multiplayer Games-Manuel
Cajada, Paulo Ferreira, Luís Veiga', :session_id =>'4', :article =>  'http://inforum.org.pt/INForum2012/docs/20120304.pdf')
Presentation.create(:start_at => DateTime.strptime('07/09/2012 17:10', '%d/%m/%Y %H:%M'),
                    :end_at =>DateTime.strptime('07/09/2012 17:40', '%d/%m/%Y %H:%M'),
                    :title =>'Cloud Agnostic Virtual Machine Image Service-João Pereira, Paula Prata',
                    :session_id =>'4', :article =>  'http://inforum.org.pt/INForum2012/docs/20120308.pdf')
Presentation.create(:start_at => DateTime.strptime('07/09/2012 17:40', '%d/%m/%Y %H:%M'),
                    :end_at =>DateTime.strptime('07/09/2012 18:00', '%d/%m/%Y %H:%M'),
                    :title =>'Sobre um Mecanismo de Sincronização baseado em Grupos de Recursos-Nuno Delgado,
Hervé Paulino', :session_id =>'4', :article =>  'http://inforum.org.pt/INForum2012/docs/20120296.pdf')



#Events
print "-- Creating Events...\n"
Event.delete_all

Event.create(:conference_id => '1', :start_at => DateTime.strptime('06/09/2012 12:45', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('06/09/2012 14:00', '%d/%m/%Y %H:%M'),
             :title =>'Opening and Turing Session', :local => 'Room 1')
Event.create(:conference_id => '1', :start_at => DateTime.strptime('06/09/2012 19:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('06/09/2012 23:00', '%d/%m/%Y %H:%M'), :title =>'Dinner',
             :local => 'Solar dos Zagalos')

###  BLANKS
Event.create(:conference_id => '1', :start_at => DateTime.strptime('06/09/2012 22:30', '%d/%m/%Y %H:%M' ),
             :end_at => DateTime.strptime('06/09/2012 23:00', '%d/%m/%Y %H:%M'),
             :title => 'Branco')
Event.create(:conference_id => '1', :start_at => DateTime.strptime('07/09/2012 22:30', '%d/%m/%Y %H:%M' ),
             :end_at => DateTime.strptime('07/09/2012 23:00', '%d/%m/%Y %H:%M'),
             :title => 'Branco')
Event.create(:conference_id => '1', :start_at => DateTime.strptime('08/09/2012 22:30', '%d/%m/%Y %H:%M' ),
             :end_at => DateTime.strptime('08/09/2012 23:00', '%d/%m/%Y %H:%M'),
             :title => 'Branco')

Schedule.find(1).events << Event.find(3)
Schedule.find(1).events << Event.find(4)
Schedule.find(1).events << Event.find(5)

#Schedule.find(1).events << Event.find(1)
#Schedule.find(2).events<< Event.find(2)

OrgMember.find(1).conferences << Conference.find(1)
OrgMember.find(1).conferences << Conference.find(2)
OrgMember.find(1).conferences << Conference.find(3)


#Author
#ActiveRecord::Base.transaction do

print "-- Creating Authors...\n"
Author.delete_all
Author.create(:presentation_id => '15', :user_id =>'36')
Author.create(:presentation_id => '15', :user_id =>'24')
Author.create(:presentation_id => '14', :user_id =>'35')
Author.create(:presentation_id => '14', :user_id =>'34')
Author.create(:presentation_id => '13', :user_id =>'33')
Author.create(:presentation_id => '13', :user_id =>'32')
Author.create(:presentation_id => '13', :user_id =>'16')
Author.create(:presentation_id => '12', :user_id =>'31')
Author.create(:presentation_id => '12', :user_id =>'30')
Author.create(:presentation_id => '12', :user_id =>'12')
Author.create(:presentation_id => '11', :user_id =>'29')
Author.create(:presentation_id => '11', :user_id =>'16')
Author.create(:presentation_id => '10', :user_id =>'28')
Author.create(:presentation_id => '10', :user_id =>'27')
Author.create(:presentation_id => '10', :user_id =>'5')
Author.create(:presentation_id => '9', :user_id =>'26')
Author.create(:presentation_id => '9', :user_id =>'21')
#Author.create(:presentation_id => '9', :user_id =>'20')
Author.create(:presentation_id => '8', :user_id =>'25')
Author.create(:presentation_id => '8', :user_id =>'24')
Author.create(:presentation_id => '8', :user_id =>'23')
Author.create(:presentation_id => '7', :user_id =>'16')
Author.create(:presentation_id => '7', :user_id =>'22')
Author.create(:presentation_id => '6', :user_id =>'21')
#Author.create(:presentation_id => '6', :user_id =>'20')
#Author.create(:presentation_id => '6', :user_id =>'19')
Author.create(:presentation_id => '5', :user_id =>'18')
Author.create(:presentation_id => '5', :user_id =>'17')
Author.create(:presentation_id => '4', :user_id =>'16')
Author.create(:presentation_id => '4', :user_id =>'15')
Author.create(:presentation_id => '3', :user_id =>'14')
Author.create(:presentation_id => '3', :user_id =>'13')
Author.create(:presentation_id => '3', :user_id =>'12')
Author.create(:presentation_id => '3', :user_id =>'11')
Author.create(:presentation_id => '2', :user_id =>'10')
Author.create(:presentation_id => '2', :user_id =>'9')
Author.create(:presentation_id => '1', :user_id =>'8')
Author.create(:presentation_id => '1', :user_id =>'7')
Author.create(:presentation_id => '1', :user_id =>'6')
Author.create(:presentation_id => '1', :user_id =>'5')

#end



#############################################################################################################
#
#                                            EURO SYS 2013
#
#############################################################################################################


print "Seeding EuroSys2013\n"

#Sessions
print "-- Creating Sessions...\n"
#Session No5
Session.create(:chair => 'Rogrigo Rodrigues', :name => 'Large scale distributed computation I',
               :conference_id =>'2', :room_id => '1')
Session.create(:chair => 'Christian Cachin', :name => 'Security and privacy',
               :conference_id =>'2', :room_id => '1')
Session.create(:chair => 'Roxanna Geambasu', :name => 'Replication',
               :conference_id =>'2', :room_id => '1')
Session.create(:chair => 'Gilles Muller', :name => 'Concurrency and parallelism',
               :conference_id =>'2', :room_id => '1')
Session.create(:chair => 'Kimberly Keeton', :name => 'Large scale distributed computation II',
               :conference_id =>'2', :room_id => '1')
Session.create(:chair => 'Tim Harris', :name => 'Operating system implementation',
               :conference_id =>'2', :room_id => '1')
Session.create(:chair => 'Ymir Vigfusson', :name => 'Miscellaneous',
               :conference_id =>'2', :room_id => '1')
Session.create(:chair => 'Steven Hand', :name => 'Virtualization',
               :conference_id =>'2', :room_id => '1')
Session.create(:chair => 'Charlie Hu', :name => 'Scheduling and performance isolation',
               :conference_id =>'2', :room_id => '1')

#Presentations
print "-- Creating Presentations...\n"
# Presentation No: 16
Presentation.create(:start_at =>DateTime.strptime('15/04/2013 08:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('15/04/2013 08:30', '%d/%m/%Y %H:%M'),
                    :title =>'TimeStream: Reliable Stream Computation in the Cloud - Zhengping Qian, Yong He, Chunzhi Su, Zhuojie Wu, and Hongyu Zhu, Taizhi Zhang, Lidong Zhou, Yuan Yu, and Zheng Zhang',
                    :session_id =>'5', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/papers-alternative/Qian.pdf')
Presentation.create(:start_at =>DateTime.strptime('15/04/2013 08:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('15/04/2013 09:00', '%d/%m/%Y %H:%M'),
                    :title =>'Optimus: A Dynamic Rewriting Framework for Execution Plans of Data-Parallel Computation - Qifa Ke, Michael Isard, and Yuan Yu',
                    :session_id =>'5', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Ke.pdf')
Presentation.create(:start_at =>DateTime.strptime('15/04/2013 09:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('15/04/2013 09:30', '%d/%m/%Y %H:%M'),
                    :title =>'BlinkDB: Queries with Bounded Errors and Bounded Response Times on Very Large Data - Sameer Agarwal, Barzan Mozafari, Aurojit Panda, Henry Milner, Samuel Madden, Ion Stoica',
                    :session_id =>'5', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/papers-alternative/Agarwal.pdf')
Presentation.create(:start_at =>DateTime.strptime('15/04/2013 10:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('15/04/2013 10:30', '%d/%m/%Y %H:%M'),
                    :title =>'IFDB: Decentralized Information Flow Control for Databases - David Schultz and Barbara Liskov',
                    :session_id =>'6', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Schultz.pdf')
# 20
Presentation.create(:start_at =>DateTime.strptime('15/04/2013 10:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('15/04/2013 11:00', '%d/%m/%Y %H:%M'),
                    :title =>'Process Firewalls: Protecting Processes During Resource Access - Hayawardh Vijayakumar, Joshua Schiffman, Trent Jaeger',
                    :session_id =>'6', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Vijayakumar.pdf')
Presentation.create(:start_at =>DateTime.strptime('15/04/2013 11:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('15/04/2013 11:30', '%d/%m/%Y %H:%M'),
                    :title =>'Resolving the conflict between generality and plausibility in verified computation - Srinath Setty, Benjamin Braun, Victor Vu, Andrew J. Blumberg, Bryan Parno, Michael Walfish',
                    :session_id =>'6', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Setty.pdf')

#Presentation.create(:start_at =>DateTime.strptime('15/04/2013 13:00', '%d/%m/%Y %H:%M') ,
#                    :end_at =>DateTime.strptime('15/04/2013 13:30', '%d/%m/%Y %H:%M'),
#                    :title =>'ChainReaction: A Causal+ Consistent Datastore based on Chain Replication - Sergio Almeida, Joao Leitao, Luıs Rodrigues',
#                   :session_id =>'7', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Almeida.pdf')

Presentation.create(:start_at =>DateTime.strptime('15/04/2013 13:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('15/04/2013 13:30', '%d/%m/%Y %H:%M'),
                    :title =>'ChainReaction: A Causal+ Consistent Datastore based on Chain Replication - Sergio Almeida',
                   :session_id =>'7', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Almeida.pdf')

Presentation.create(:start_at =>DateTime.strptime('15/04/2013 13:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('15/04/2013 14:00', '%d/%m/%Y %H:%M'),
                    :title =>'Augustus: Scalable and Robust Storage for Cloud Applications - Ricardo Padilha and Fernando Pedone',
                    :session_id =>'7', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Padilha.pdf')
Presentation.create(:start_at =>DateTime.strptime('15/04/2013 14:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('15/04/2013 14:30', '%d/%m/%Y %H:%M'),
                    :title =>'MDCC: Multi-Data Center Consistency - Tim Kraska, Gene Pang, Michael Franklin, Samuel Madden, Alan Fekete',
                    :session_id =>'7', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Kraska.pdf')
# 25
Presentation.create(:start_at =>DateTime.strptime('15/04/2013 15:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('15/04/2013 15:30', '%d/%m/%Y %H:%M'),
                    :title =>'Conversion: Multi-Version Concurrency Control for Main Memory Segments - Timothy Merrifield, Jakob Eriksson',
                    :session_id =>'8', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Merryfield.pdf')
Presentation.create(:start_at =>DateTime.strptime('15/04/2013 15:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('15/04/2013 16:00', '%d/%m/%Y %H:%M'),
                    :title =>'Whose Cache Line Is It Anyway? Operating System Support for Live Detection and Repair of False Sharing - Mihir Nanavati, Mark Spear, Nathan Taylor, Shriram Rajagopalan, Dutch T. Meyer, William Aiello, Andrew Warfield',
                    :session_id =>'8', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Nanavati.pdf')
Presentation.create(:start_at =>DateTime.strptime('15/04/2013 15:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('15/04/2013 16:00', '%d/%m/%Y %H:%M'),
                    :title =>'Adaptive Parallelism for Web Search - Myeongjae Jeon, Yuxiong He, Sameh Elnikety, Alan L. Cox, Scott Rixner',
                    :session_id =>'8', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Jeon.pdf')
Presentation.create(:start_at =>DateTime.strptime('16/04/2013 08:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('16/04/2013 08:30', '%d/%m/%Y %H:%M'),
                    :title =>'Mizan: A System for Dynamic Load Balancing in Large-scale Graph Processing - Zuhair Khayyat, Karim Awara, Amani Alonazi, Hani Jamjoom Dan Williams, Panos Kalnis',
                    :session_id =>'9', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Kayyat.pdf')
Presentation.create(:start_at =>DateTime.strptime('16/04/2013 08:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('16/04/2013 09:00', '%d/%m/%Y %H:%M'),
                    :title =>'MeT: Workload aware elasticity for NoSQL - Francisco Cruz, Francisco Maia, Miguel Matos, Rui Oliveira, Joao Paulo, Jose Pereira, Ricardo Vilaca',
                    :session_id =>'9', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Cruz.pdf')
# 30
Presentation.create(:start_at =>DateTime.strptime('16/04/2013 10:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('16/04/2013 10:30', '%d/%m/%Y %H:%M'),
                    :title =>'RadixVM: Scalable address spaces for multithreaded applications - Austin T. Clements, Frans Kaashoek, Nickolai Zeldovich',
                    :session_id =>'10', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Clements.pdf')
Presentation.create(:start_at =>DateTime.strptime('16/04/2013 10:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('16/04/2013 11:00', '%d/%m/%Y %H:%M'),
                    :title =>'Failure-Atomic msync(): A Simple and Efficient Mechanism for Preserving the Integrity of Durable Data - Stan Park, Terence Kelly, Kai Shen',
                    :session_id =>'10', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Park.pdf')
Presentation.create(:start_at =>DateTime.strptime('16/04/2013 11:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('16/04/2013 11:30', '%d/%m/%Y %H:%M'),
                    :title =>'Composing OS extensions safely and efficiently with Bascule - Andrew Baumann, Dongyoon Lee, Pedro Fonseca, Jacob R. Lorch, Barry Bond, Reuben Olinsky, Galen C. Hunt',
                    :session_id =>'10', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Baumann.pdf')
Presentation.create(:start_at =>DateTime.strptime('16/04/2013 13:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('16/04/2013 13:30', '%d/%m/%Y %H:%M'),
                    :title =>'Hypnos: Understanding and Treating Sleep Conflicts in Smartphone - Abhilash Jindal, Abhinav Pathak, Y. Charlie Hu, Samuel Midkiff',
                    :session_id =>'11', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Jindal.pdf')
Presentation.create(:start_at =>DateTime.strptime('16/04/2013 13:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('16/04/2013 14:00', '%d/%m/%Y %H:%M'),
                    :title =>'Prefetching Mobile Ads: Can advertising systems afford it? - Prashanth Mohan, Suman Nath, Oriana Riva',
                    :session_id =>'11', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Mohan.pdf')
# 35
Presentation.create(:start_at =>DateTime.strptime('16/04/2013 14:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('16/04/2013 14:30', '%d/%m/%Y %H:%M'),
                    :title =>'Maygh:Building a CDN from client web browsers - Liang Zhang, Fangfei Zhou, Alan Mislove, Ravi Sundaram',
                    :session_id =>'11', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Zhang_1.pdf')
Presentation.create(:start_at =>DateTime.strptime('16/04/2013 14:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('16/04/2013 15:00', '%d/%m/%Y %H:%M'),
                    :title =>'A Compiler-level Intermediate Representation based Binary Analysis and Rewriting System - Kapil Anand, Matthew Smithson, Khaled Elwazeer, Aparna Kotha, Jim Gruen, Nathan Giles, Rajeev Barua',
                    :session_id =>'11', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Anand.pdf')
Presentation.create(:start_at =>DateTime.strptime('17/04/2013 08:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('17/04/2013 08:30', '%d/%m/%Y %H:%M'),
                    :title =>'hClock: Hierarchical QoS for Packet Scheduling in a Hypervisor - Jean-Pascal Billaud, Ajay Gulati',
                    :session_id =>'12', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Billaud.pdf')
Presentation.create(:start_at =>DateTime.strptime('17/04/2013 08:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('17/04/2013 09:00', '%d/%m/%Y %H:%M'),
                    :title =>'RapiLog: Reducing System Complexity Through Verification - Gernot Heiser, Etienne Le Sueur, Adrian Danis, Aleksander Budzynowski, Tudor-Ioan Salomie, Gustavo Alonso',
                    :session_id =>'12', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Heiser.pdf')
Presentation.create(:start_at =>DateTime.strptime('17/04/2013 09:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('17/04/2013 09:30', '%d/%m/%Y %H:%M'),
                    :title =>'Application Level Ballooning for Efficient Server Consolidation - Tudor-Ioan Salomie, Gustavo Alonso, Timothy Roscoe, Kevin Elphinstone',
                    :session_id =>'12', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Salomie.pdf')
# 40
Presentation.create(:start_at =>DateTime.strptime('17/04/2013 10:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('17/04/2013 10:30', '%d/%m/%Y %H:%M'),
                    :title =>'Omega: flexible, scalable schedulers for large compute clusters - Malte Schwarzkopf, Andy Konwinski, Michael Abd-el-Malek, John Wilkes',
                    :session_id =>'13', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Schwarzkopf.pdf')
Presentation.create(:start_at =>DateTime.strptime('17/04/2013 11:00', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('17/04/2013 11:30', '%d/%m/%Y %H:%M'),
                    :title =>'Choosy: Max-Min Fair Sharing for Datacenter Jobs with Constraints - Ali Ghodsi, Matei Zaharia, Scott Shenker, Ion Stoica',
                    :session_id =>'13', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Ghodsi.pdf')
Presentation.create(:start_at =>DateTime.strptime('17/04/2013 10:30', '%d/%m/%Y %H:%M') ,
                    :end_at =>DateTime.strptime('17/04/2013 11:00', '%d/%m/%Y %H:%M'),
                    :title =>'CPI2: CPU performance isolation for shared compute clusters - Xiao Zhang, Eric Tune, Robert Hagmann, Rohit Jnagal, Vrigo Gokhale, John Wilkes',
                    :session_id =>'13', :article => 'http://eurosys2013.tudos.org/wp-content/uploads/2013/paper/Zhang_2.pdf')



print "-- Creating Events...\n"
Event.create(:conference_id => '2', :start_at => DateTime.strptime('15/04/2013 07:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('15/04/2013 08:00', '%d/%m/%Y %H:%M'),
             :title =>'Opening', :local => 'Room 1')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('16/04/2013 15:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('16/04/2013 17:00', '%d/%m/%Y %H:%M'), :title =>'Poster Session',
             :local => 'Room 1')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('17/04/2013 11:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('17/04/2013 12:00', '%d/%m/%Y %H:%M'), :title =>'Closing and presentation of best paper awards',
             :local => 'Room 1')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('14/04/2013 11:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('14/04/2013 12:30', '%d/%m/%Y %H:%M'), :title =>'Lunch', :local => 'Room 1')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('15/04/2013 11:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('15/04/2013 12:30', '%d/%m/%Y %H:%M'), :title =>'Workshop', :local => 'Room 1')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('16/04/2013 11:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('16/04/2013 12:30', '%d/%m/%Y %H:%M'), :title =>'Lunch',
             :local => 'Room 1')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('15/04/2013 11:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('15/04/2013 12:30', '%d/%m/%Y %H:%M'), :title =>'Lunch',
             :local => 'Room 1')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('14/04/2013 07:00', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('14/04/2013 07:30', '%d/%m/%Y %H:%M'), :title =>'Registration',
             :local => 'Loby')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('15/04/2013 07:00', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('15/04/2013 07:30', '%d/%m/%Y %H:%M'), :title =>'Registration',
             :local => 'Loby')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('16/04/2013 07:15', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('16/04/2013 07:45', '%d/%m/%Y %H:%M'), :title =>'Registration',
             :local => 'Loby')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('17/04/2013 07:15', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('17/04/2013 07:45', '%d/%m/%Y %H:%M'), :title =>'Registration',
             :local => 'Loby')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('14/04/2013 09:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('14/04/2013 10:00', '%d/%m/%Y %H:%M'), :title =>'Coffe Break',
             :local => 'Loby')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('15/04/2013 09:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('15/04/2013 10:00', '%d/%m/%Y %H:%M'), :title =>'Coffe Break',
             :local => 'Loby')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('16/04/2013 09:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('16/04/2013 10:00', '%d/%m/%Y %H:%M'), :title =>'Coffe Break',
             :local => 'Loby')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('17/04/2013 09:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('17/04/2013 10:00', '%d/%m/%Y %H:%M'), :title =>'Coffe Break',
             :local => 'Loby')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('14/04/2013 14:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('14/04/2013 15:00', '%d/%m/%Y %H:%M'), :title =>'Coffe Break',
             :local => 'Loby')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('15/04/2013 14:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('15/04/2013 15:00', '%d/%m/%Y %H:%M'), :title =>'Coffe Break',
             :local => 'Loby')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('16/04/2013 15:00', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('16/04/2013 15:30', '%d/%m/%Y %H:%M'), :title =>'Coffe Break',
             :local => 'Loby')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('16/04/2013 18:30', '%d/%m/%Y %H:%M'),
             :end_at =>DateTime.strptime('16/04/2013 21:30', '%d/%m/%Y %H:%M'), :title =>'Banquet Dinner in old town',
             :local => 'Old Town')



Event.create(:conference_id => '2', :start_at => DateTime.strptime('14/04/2013 22:30', '%d/%m/%Y %H:%M' ),
             :end_at => DateTime.strptime('14/04/2013 23:00', '%d/%m/%Y %H:%M'),
             :title => 'Branco')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('15/04/2013 22:30', '%d/%m/%Y %H:%M' ),
             :end_at => DateTime.strptime('15/04/2013 23:00', '%d/%m/%Y %H:%M'),
             :title => 'Branco')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('16/04/2013 22:30', '%d/%m/%Y %H:%M' ),
             :end_at => DateTime.strptime('16/04/2013 23:00', '%d/%m/%Y %H:%M'),
             :title => 'Branco')
Event.create(:conference_id => '2', :start_at => DateTime.strptime('17/04/2013 22:30', '%d/%m/%Y %H:%M' ),
             :end_at => DateTime.strptime('17/04/2013 23:00', '%d/%m/%Y %H:%M'),
             :title => 'Branco')

Schedule.find(2).events << Event.find(25)
Schedule.find(2).events << Event.find(26)
Schedule.find(2).events << Event.find(27)
Schedule.find(2).events << Event.find(28)


print "-- Creating Institution...\n"
#Institution No 8:
Institution.create(:name=> 'Microsoft Research Asia')
Institution.create(:name=> 'South China University of Technology')
#10:
Institution.create(:name=> 'Shanghai Jiaotong University')
Institution.create(:name=> 'Peking University')
Institution.create(:name=> 'Microsoft Research Silicon Valley')
Institution.create(:name=> 'University of California, Berkeley')
Institution.create(:name=> 'Massachusetts Institute of Technology')
#15:
Institution.create(:name=> 'MIT CSAIL')
Institution.create(:name=> 'The Pennsylvania State University')
Institution.create(:name=> 'Advanced Micro Devices')
Institution.create(:name=> 'UT Austin')
Institution.create(:name=> 'Microsoft Research Redmond)')
#20:
Institution.create(:name=> 'University of Lugano, Switzerland')
Institution.create(:name=> 'University of Sydney')
Institution.create(:name=> 'University of Illinois at Chicago')
Institution.create(:name=> 'University of British Columbia')
Institution.create(:name=> 'Rice University')
#25
Institution.create(:name=> 'Microsoft Research')
Institution.create(:name=> 'King Abdullah University of Science and Technology')
Institution.create(:name=> 'IBM T. J. Watson Research Center, Yorktown Heights')
Institution.create(:name=> 'HASLab / INESC TEC and U. Minho')
Institution.create(:name=> 'HP Labs')
#30
Institution.create(:name=> 'University of Rochester')
Institution.create(:name=> 'University of Michigan')
Institution.create(:name=> 'MPI Software Systems')
Institution.create(:name=> 'Purdue University')
Institution.create(:name=> 'Northeastern University')
#35
Institution.create(:name=> 'University of Maryland, College Park')
Institution.create(:name=> 'VMware, Inc.')
Institution.create(:name=> 'NICTA and UNSW')
Institution.create(:name=> 'ETH Zurich')
Institution.create(:name=> 'University of Cambridge Computer Laboratory')
#40
Institution.create(:name=> 'Google Inc.')

print "-- Creating Users...\n"
#User No 33
User.create(:firstName => 'Zhengping', :lastName => 'Qian', :email => 'zheqian@microsoft.com', :institution_id =>8, :password => 'password')
User.create(:firstName => 'Yong', :lastName => 'He', :email => 'yonghe@microsoft.com', :institution_id =>8, :password => 'password')
# 35
User.create(:firstName => 'Chunzhi', :lastName => 'Su', :email => 'chusu@microsoft.com', :institution_id =>11, :password => 'password')
User.create(:firstName => 'Zhuojie', :lastName => 'Wu', :email => 'zhuwu@microsoft.com',  :institution_id =>9, :password => 'password')
User.create(:firstName => 'Hongyu', :lastName => 'Zhu', :email => 'honzhu@microsoft.com', :institution_id =>10, :password => 'password')
User.create(:firstName => 'Taizhi', :lastName => 'Zhang', :email => 'taizhang@microsoft.com', :institution_id =>8, :password => 'password')
User.create(:firstName => 'Lidong', :lastName => 'Zhou', :email => 'lizhou@microsoft.com', :institution_id =>8, :password => 'password')
# 40
User.create(:firstName => 'Yuan', :lastName => 'Yu', :photo => 'http://research.microsoft.com/en-us/news/features/images/yuanbyu_storylevel.jpg', :email => 'yuanyu@microsoft.com',  :institution_id =>8, :password => 'password')
User.create(:firstName => 'Zheng', :lastName => 'Zhang', :email => 'zhenzha@microsoft.com', :institution_id =>9, :password => 'password')
User.create(:firstName => 'Qifa', :lastName => 'Ke', :email => 'qifake@microsoft.com', :institution_id =>12, :password => 'password')
User.create(:firstName => 'Michael', :lastName => 'Isard', :email => 'michaelsard@microsoft.com', :institution_id =>12, :password => 'password')
User.create(:firstName => 'Barzan', :lastName => 'Mozafari ', :email => 'barzanmozafari@mit.edu ', :institution_id =>14 , :password => 'password')
# 45
User.create(:firstName => 'Sameer', :lastName => 'Agarwal ', :email => 'sameeragarwal@caluni.edu', :institution_id =>13 , :password => 'password')
User.create(:firstName => 'Aurojit', :lastName => 'Panda', :email => 'panda@caluni.edu', :institution_id =>13, :password => 'password')
User.create(:firstName => 'Henry', :lastName => 'Milner', :email => 'milner@caluni.edu', :institution_id =>13 , :password => 'password')
User.create(:firstName => 'Samuel', :lastName => 'Madden', :email => 'madden@mit.edu', :institution_id =>14 , :password => 'password')
User.create(:firstName => 'Ion', :lastName => 'Stoica', :email => 'ionstoica@caluni.edu', :institution_id =>13 , :password => 'password')
#50
User.create(:firstName => 'David', :lastName => 'Schultz', :email => 'schultz@mit.edu', :institution_id =>15 , :password => 'password')
User.create(:firstName => 'Barbara', :lastName => 'Liskov', :photo => 'http://www.pmg.lcs.mit.edu/~liskov/images/LISKOV_crop2.jpg', :email => 'barbaraliskov@mit.edu', :institution_id =>15 , :password => 'password')
User.create(:firstName => 'Hayawardh', :lastName => 'Vijayakumar', :email => 'hvijay@cse.psu.edu', :institution_id =>16 , :password => 'password')
User.create(:firstName => 'Joshua', :lastName => 'Schiffman', :email => 'man@amd.com', :institution_id =>17 , :password => 'password')
User.create(:firstName => 'Trent', :lastName => 'Jaeger', :email => 'tjaeger@cse.psu.edu', :institution_id =>16 , :password => 'password')
#55
User.create(:firstName => 'Srinath', :lastName => 'Setty', :email => 'setty@utaustin.edu', :institution_id =>18 , :password => 'password')
User.create(:firstName => 'Benjamin', :lastName => 'Braun', :email => 'bbraun@utaustin.edu', :institution_id =>18 , :password => 'password')
User.create(:firstName => 'Victor', :lastName => 'Vu', :email => 'victorvu@utaustin.edu', :institution_id =>18 , :password => 'password')
User.create(:firstName => 'Andrew J.', :lastName => 'Blumberg', :email => 'blumberg@utaustin.edu', :institution_id =>18 , :password => 'password')
User.create(:firstName => 'Bryan', :lastName => 'Parno', :email => 'bryanparno@microsoft.com', :institution_id =>19 , :password => 'password')
# 60
User.create(:firstName => 'Michael', :lastName => 'Walfish', :email => 'walfish@utaustin.edu', :institution_id =>18 , :password => 'password')
User.create(:firstName => 'Ricardo', :lastName => 'Padilha', :email => 'padilha@louni.sw', :institution_id =>20 , :password => 'password')
User.create(:firstName => 'Fernando', :lastName => 'Pedone', :email => 'pedone@louni.sw', :institution_id =>20 , :password => 'password')
User.create(:firstName => 'Tim', :lastName => 'Kraska', :email => 'kraska@ucberkley.edu', :institution_id =>13 , :password => 'password')
User.create(:firstName => 'Michael', :lastName => 'Franklin', :email => 'franklin@ucberkley.edu', :institution_id =>13 , :password => 'password')
# 65
User.create(:firstName => 'Gene', :lastName => 'Pang', :email => 'genepang@ucberkley.edu', :institution_id =>13 , :password => 'password')
User.create(:firstName => 'Samuel', :lastName => 'Madden', :email => 'samuelmadden@mit.edu', :institution_id =>14 , :password => 'password')
User.create(:firstName => 'Alan', :lastName => 'Fekete', :email => 'fekete@sydneyuni.au', :institution_id =>21 , :password => 'password')
User.create(:firstName => 'Timothy', :lastName => 'Merrifield', :email => 'merrifield@iluni.edu', :institution_id =>22 , :password => 'password')
User.create(:firstName => 'Jakob', :lastName => 'Eriksson', :email => 'jakoberiksson@iliuni.edu', :institution_id =>22 , :password => 'password')
# 70
User.create(:firstName => 'Mihir,', :lastName => 'Nanavati', :email => 'nanavati@cs.ubc.ca', :institution_id =>23 , :password => 'password')
User.create(:firstName => 'Mark', :lastName => 'Spear', :email => 'markspear@cs.ubc.ca', :institution_id =>23 , :password => 'password')
User.create(:firstName => 'Nathan', :lastName => 'Taylor', :email => 'nathantaylor@cs.ubc.ca', :institution_id =>23 , :password => 'password')
User.create(:firstName => 'Shriram', :lastName => 'Rajagopalan', :email => 'rajagopalan@cs.ubc.ca', :institution_id =>23 , :password => 'password')
User.create(:firstName => 'Dutch T.', :lastName => 'Meyer', :email => 'dteyer@cs.ubc.ca', :institution_id =>23 , :password => 'password')
# 75
User.create(:firstName => 'William', :lastName => 'Aiello', :email => 'aiello@cs.ubc.ca', :institution_id =>23 , :password => 'password')
User.create(:firstName => 'Andrew', :lastName => 'Warfield', :email => 'a.warfield@cs.ubc.ca', :institution_id =>23 , :password => 'password')
User.create(:firstName => 'Myeongjae', :lastName => 'Jeon', :email => 'myjeon@rice.ch', :institution_id =>24 , :password => 'password')
User.create(:firstName => 'Yuxiong', :lastName => 'He', :email => 'yuxionghe@microsoft.com', :institution_id =>25 , :password => 'password')
User.create(:firstName => 'Sameh', :lastName => 'Elnikety', :email => 'elnikety@microsoft.com', :institution_id =>25 , :password => 'password')
#80
User.create(:firstName => 'Alan L.', :lastName => 'Cox', :email => 'alanlcox@rice.cx', :institution_id =>24, :password => 'password')
User.create(:firstName => 'Scott', :lastName => 'Rixner', :email => 'scottrixner@rice.ch', :institution_id =>24 , :password => 'password')
User.create(:firstName => 'Zuhair', :lastName => 'Khayyat', :email => 'khayyat@kauni.com', :institution_id =>26 , :password => 'password')
User.create(:firstName => 'Karim', :lastName => 'Awara', :email => 'kawara@kauni.com', :institution_id =>26 , :password => 'password')
User.create(:firstName => 'Amani', :lastName => 'Alonazi', :email => 'aalonazi@kauni.com', :institution_id =>26 , :password => 'password')
#85
User.create(:firstName => 'Hani', :lastName => 'Jamjoom', :email => 'hanijamjoom@ibm.com', :institution_id =>27 , :password => 'password')
User.create(:firstName => 'Dan', :lastName => 'Williams', :email => 'danwilliams@ibm.com', :institution_id =>27 , :password => 'password')
User.create(:firstName => 'Panos', :lastName => 'Kalnis', :email => 'panoskalnis@kauni.com', :institution_id =>26 , :password => 'password')
User.create(:firstName => 'Francisco', :lastName => 'Cruz', :email => 'franciscocruz@di.uminho.pt', :institution_id =>28 , :password => 'password')
User.create(:firstName => 'Francisco', :lastName => 'Maia', :email => 'franciscomaia@di.uminho.pt', :institution_id =>28 , :password => 'password')
#90
User.create(:firstName => 'Miguel', :lastName => 'Matos', :email => 'miguelmatos@di.uminho.pt', :institution_id =>28 , :password => 'password')
User.create(:firstName => 'Rui', :lastName => 'Oliveira', :email => 'ruioliveira@di.uminho.pt', :institution_id =>28 , :password => 'password')
User.create(:firstName => 'Joao', :lastName => 'Paulo', :email => 'joaopaulo@di.uminho.pt', :institution_id =>28 , :password => 'password')
User.create(:firstName => 'Jose', :lastName => 'Pereira', :email => 'josepereira@uminho.pt', :institution_id =>28 , :password => 'password')
User.create(:firstName => 'Ricardo', :lastName => 'Vilaca', :email => 'ricardovilaca@di.uminho.pt', :institution_id =>28 , :password => 'password')
#95
User.create(:firstName => 'Shivaram', :lastName => 'Venkataraman', :email => 'venkataraman@uniberkley', :institution_id =>13 , :password => 'password')
User.create(:firstName => 'Erik', :lastName => 'Bodzsar', :email => 'bodzsar@unico.edu', :institution_id =>22 , :password => 'password')
User.create(:firstName => 'Indrajit', :lastName => 'Roy', :email => 'indroy@hp.com', :institution_id =>29 , :password => 'password')
User.create(:firstName => 'Alvin', :lastName => 'AuYoung', :email => 'alvinauyoung@hp.com', :institution_id =>29 , :password => 'password')
User.create(:firstName => 'Robert S.', :lastName => 'Schreiber', :email => 'schreiber@hp.com', :institution_id =>29 , :password => 'password')
#100
User.create(:firstName => 'Austin T.', :lastName => 'Clements', :email => 'austinclements@mit.edu', :institution_id =>15 , :password => 'password')
User.create(:firstName => 'Frans', :lastName => 'Kaashoek', :email => 'kaashoek@mit.edu', :institution_id =>15 , :password => 'password')
User.create(:firstName => 'Nickolai', :lastName => 'Zeldovich', :email => 'zeldovich@mit.edu', :institution_id =>15 , :password => 'password')
User.create(:firstName => 'Stan', :lastName => 'Park', :email => 'stanpark@rochuni.edu', :institution_id =>30 , :password => 'password')
User.create(:firstName => 'Terence', :lastName => 'Kelly', :email => 'terencekelly@hp.com', :institution_id =>29 , :password => 'password')
#105
User.create(:firstName => 'Kai', :lastName => 'Shen', :email => 'kaishan@rosh.edu', :institution_id =>30 , :password => 'password')
User.create(:firstName => 'Andrew', :lastName => 'Baumann', :email => 'baumann@microsoft.com', :institution_id =>25 , :password => 'password')
User.create(:firstName => 'Dongyoon', :lastName => 'Lee', :email => 'dongyoonlee@michiganuni.edu', :institution_id =>31 , :password => 'password')
User.create(:firstName => 'Pedro', :lastName => 'Fonseca', :email => 'pedroonseca@mpisys.com', :institution_id =>32 , :password => 'password')
User.create(:firstName => 'Jacob R.', :lastName => 'Lorch', :email => 'lorch@microsoft.com', :institution_id =>25 , :password => 'password')
#110
User.create(:firstName => 'Barry', :lastName => 'Bond', :email => 'bbond@microsoft.com', :institution_id =>25 , :password => 'password')
User.create(:firstName => 'Reuben', :lastName => 'Olinsky', :email => 'olinsky@microsoft.com', :institution_id =>25 , :password => 'password')
User.create(:firstName => 'Galen C.', :lastName => 'Hunt', :email => 'galenhunt@microsoft.com', :institution_id =>25 , :password => 'password')
User.create(:firstName => 'Abhilash', :lastName => 'Jindal', :email => 'jindal@purdue.edu', :institution_id =>33 , :password => 'password')
User.create(:firstName => 'Abhinav', :lastName => 'Pathak', :email => 'pathak@purdue.edu', :institution_id =>33 , :password => 'password')
#115
User.create(:firstName => 'Y. Charlie', :lastName => 'Hu', :email => 'yhu@purdue.edu', :institution_id =>33 , :password => 'password')
User.create(:firstName => 'Samuel', :lastName => 'Midkiff', :email => 'midkiff', :institution_id =>33 , :password => 'password')
User.create(:firstName => 'Prashanth', :lastName => 'Mohan', :email => 'mohan@ucberkley.edu', :institution_id =>13 , :password => 'password')
User.create(:firstName => 'Suman', :lastName => 'Nath', :email => 'snath@microsoft.com', :institution_id =>25 , :password => 'password')
User.create(:firstName => 'Oriana', :lastName => 'Riva', :email => 'riva@microsoft.com', :institution_id =>25 , :password => 'password')
#120
User.create(:firstName => 'Liang', :lastName => 'Zhang', :email => 'zhang@northuni.edu', :institution_id =>34 , :password => 'password')
User.create(:firstName => 'Fangfei', :lastName => 'Zhou', :email => 'zhou@northuni.edu', :institution_id =>34 , :password => 'password')
User.create(:firstName => 'Alan', :lastName => 'Mislove', :email => 'mislove@northuni.edu', :institution_id =>34 , :password => 'password')
User.create(:firstName => 'Ravi', :lastName => 'Sundaram', :email => 'sundaram@northuni.edu', :institution_id =>34 , :password => 'password')
User.create(:firstName => 'Kapil', :lastName => 'Anand', :email => 'anand@myuni.edu', :institution_id =>35 , :password => 'password')
#125
User.create(:firstName => 'Matthew', :lastName => 'Smithson', :email => 'smithson@myuni.edu', :institution_id =>35 , :password => 'password')
User.create(:firstName => 'Khaled', :lastName => 'Elwazeer', :email => 'elwazeer@myuni.edu', :institution_id =>35 , :password => 'password')
User.create(:firstName => 'Aparna', :lastName => 'Kotha', :email => 'kotha@myuni.edu', :institution_id =>35 , :password => 'password')
User.create(:firstName => 'Jim', :lastName => 'Gruen', :email => 'gruen@myuni.edu', :institution_id =>35 , :password => 'password')
User.create(:firstName => 'Nathan', :lastName => 'Giles', :email => 'giles@myuni.edu', :institution_id =>35 , :password => 'password')
#130
User.create(:firstName => 'Rajeev', :lastName => 'Barua', :email => 'barua@myuni.edu', :institution_id =>35 , :password => 'password')
User.create(:firstName => 'Jean-Pascal', :lastName => 'Billaud', :email => 'billaud@vmware.com', :institution_id =>36 , :password => 'password')
User.create(:firstName => 'Ajay', :lastName => 'Gulati', :email => 'gulati@vmware.com', :institution_id =>36 , :password => 'password')
User.create(:firstName => 'Gernot', :lastName => 'Heiser', :email => 'heiser@nicta.edu', :institution_id =>37 , :password => 'password')
User.create(:firstName => 'Etienne', :lastName => 'Le Sueur', :email => 'sueur@nicta.edu', :institution_id =>37 , :password => 'password')
#135
User.create(:firstName => 'Adrian', :lastName => 'Danis', :email => 'adanis@nicta.edu', :institution_id =>37 , :password => 'password')
User.create(:firstName => 'Aleksander', :lastName => 'Budzynowski', :email => 'budzynowski@nicta.edu', :institution_id =>37 , :password => 'password')
User.create(:firstName => 'Tudor-Ioan', :lastName => 'Salomie', :email => 'salomie@eth.sw', :institution_id =>38 , :password => 'password')
User.create(:firstName => 'Gustavo', :lastName => 'Alonso', :email => 'alonso@eth.sw', :institution_id =>38 , :password => 'password')
User.create(:firstName => 'Timothy', :lastName => 'Roscoe', :email => 'tiroscoe@eth.sw', :institution_id =>38 , :password => 'password')
#140
User.create(:firstName => 'Kevin', :lastName => 'Elphinstone', :email => 'elphinstone@unsw.sw', :institution_id =>37 , :password => 'password')
User.create(:firstName => 'Malte', :lastName => 'Schwarzkopf', :email => 'schwarzkopf@cambridge.edu', :institution_id =>39 , :password => 'password')
User.create(:firstName => 'Andy', :lastName => 'Konwinski', :email => 'konwinski@berkeley.edu', :institution_id =>13 , :password => 'password')
User.create(:firstName => 'Michael', :lastName => 'Abd-el-Malek', :email => 'abdelmalek@google.com', :institution_id =>40 , :password => 'password')
User.create(:firstName => 'John', :lastName => 'Wilkes', :email => 'wilkes@google.com', :institution_id =>40 , :password => 'password')
#145
User.create(:firstName => 'Ali', :lastName => 'Ghodsi', :email => 'ghodsi@berkeley.edu', :institution_id =>13 , :password => 'password')
User.create(:firstName => 'Matei', :lastName => 'Zaharia', :email => 'zaharia@berkeley.edu', :institution_id =>13 , :password => 'password')
User.create(:firstName => 'Scott', :lastName => 'Shenker', :email => 'shenker@berkeley.edu', :institution_id =>13 , :password => 'password')
User.create(:firstName => 'Ion', :lastName => 'Stoica', :email => 'stoica@berkeley.edu', :institution_id =>13 , :password => 'password')
User.create(:firstName => 'Xiao', :lastName => 'Zhang', :email => 'zhang@google.com', :institution_id =>40 , :password => 'password')
#150
User.create(:firstName => 'Eric', :lastName => 'Tune', :email => 'erictune@gogle.com', :institution_id =>40 , :password => 'password')
User.create(:firstName => 'Robert', :lastName => 'Hagmann', :email => 'roberthagmann@google.com', :institution_id =>40 , :password => 'password')
User.create(:firstName => 'Rohit', :lastName => 'Jnagal', :email => 'jnagal@google.com', :institution_id =>40 , :password => 'password')
User.create(:firstName => 'Vrigo', :lastName => 'Gokhale', :email => 'gokhale@google.com', :institution_id =>40 , :password => 'password')




print "-- Creating Authors...\n"

#Author No 37
Author.create(:presentation_id => '16', :user_id =>'33')
Author.create(:presentation_id => '16', :user_id =>'34')
Author.create(:presentation_id => '16', :user_id =>'35')
Author.create(:presentation_id => '16', :user_id =>'36')
Author.create(:presentation_id => '16', :user_id =>'37')
Author.create(:presentation_id => '16', :user_id =>'38')
Author.create(:presentation_id => '16', :user_id =>'39')
Author.create(:presentation_id => '16', :user_id =>'40')
Author.create(:presentation_id => '16', :user_id =>'41')
Author.create(:presentation_id => '17', :user_id =>'42')
Author.create(:presentation_id => '17', :user_id =>'43')
Author.create(:presentation_id => '17', :user_id =>'40')
Author.create(:presentation_id => '18', :user_id =>'44')
Author.create(:presentation_id => '18', :user_id =>'45')
Author.create(:presentation_id => '18', :user_id =>'46')
Author.create(:presentation_id => '18', :user_id =>'47')
Author.create(:presentation_id => '18', :user_id =>'48')
Author.create(:presentation_id => '18', :user_id =>'49')
Author.create(:presentation_id => '19', :user_id =>'50')
Author.create(:presentation_id => '19', :user_id =>'51')
Author.create(:presentation_id => '20', :user_id =>'52')
Author.create(:presentation_id => '20', :user_id =>'53')
Author.create(:presentation_id => '20', :user_id =>'54')
Author.create(:presentation_id => '21', :user_id =>'55')
Author.create(:presentation_id => '21', :user_id =>'56')
Author.create(:presentation_id => '21', :user_id =>'57')
Author.create(:presentation_id => '21', :user_id =>'58')
Author.create(:presentation_id => '21', :user_id =>'59')
Author.create(:presentation_id => '21', :user_id =>'60')
Author.create(:presentation_id => '22', :user_id =>'25')
#Author.create(:presentation_id => '22', :user_id =>'20')
Author.create(:presentation_id => '22', :user_id =>'21')
Author.create(:presentation_id => '23', :user_id =>'61')
Author.create(:presentation_id => '23', :user_id =>'62')
Author.create(:presentation_id => '24', :user_id =>'63')
Author.create(:presentation_id => '24', :user_id =>'64')
Author.create(:presentation_id => '24', :user_id =>'65')
Author.create(:presentation_id => '24', :user_id =>'66')
Author.create(:presentation_id => '24', :user_id =>'67')
Author.create(:presentation_id => '25', :user_id =>'68')
Author.create(:presentation_id => '25', :user_id =>'69')
Author.create(:presentation_id => '26', :user_id =>'70')
Author.create(:presentation_id => '26', :user_id =>'71')
Author.create(:presentation_id => '26', :user_id =>'72')
Author.create(:presentation_id => '26', :user_id =>'73')
Author.create(:presentation_id => '26', :user_id =>'74')
Author.create(:presentation_id => '26', :user_id =>'75')
Author.create(:presentation_id => '26', :user_id =>'76')
Author.create(:presentation_id => '27', :user_id =>'77')
Author.create(:presentation_id => '27', :user_id =>'78')
Author.create(:presentation_id => '27', :user_id =>'79')
Author.create(:presentation_id => '27', :user_id =>'80')
Author.create(:presentation_id => '27', :user_id =>'81')
Author.create(:presentation_id => '28', :user_id =>'82')
Author.create(:presentation_id => '28', :user_id =>'83')
Author.create(:presentation_id => '28', :user_id =>'84')
Author.create(:presentation_id => '28', :user_id =>'85')
Author.create(:presentation_id => '28', :user_id =>'86')
Author.create(:presentation_id => '28', :user_id =>'87')
Author.create(:presentation_id => '29', :user_id =>'88')
Author.create(:presentation_id => '29', :user_id =>'89')
Author.create(:presentation_id => '29', :user_id =>'90')
Author.create(:presentation_id => '29', :user_id =>'91')
Author.create(:presentation_id => '29', :user_id =>'92')
Author.create(:presentation_id => '29', :user_id =>'93')
Author.create(:presentation_id => '29', :user_id =>'94')
Author.create(:presentation_id => '30', :user_id =>'95')
Author.create(:presentation_id => '30', :user_id =>'96')
Author.create(:presentation_id => '30', :user_id =>'97')
Author.create(:presentation_id => '30', :user_id =>'98')
Author.create(:presentation_id => '30', :user_id =>'99')
Author.create(:presentation_id => '31', :user_id =>'100')
Author.create(:presentation_id => '31', :user_id =>'101')
Author.create(:presentation_id => '31', :user_id =>'102')
Author.create(:presentation_id => '32', :user_id =>'103')
Author.create(:presentation_id => '32', :user_id =>'104')
Author.create(:presentation_id => '32', :user_id =>'105')
Author.create(:presentation_id => '33', :user_id =>'106')
Author.create(:presentation_id => '33', :user_id =>'107')
Author.create(:presentation_id => '33', :user_id =>'108')
Author.create(:presentation_id => '33', :user_id =>'109')
Author.create(:presentation_id => '33', :user_id =>'110')
Author.create(:presentation_id => '33', :user_id =>'111')
Author.create(:presentation_id => '33', :user_id =>'112')
Author.create(:presentation_id => '34', :user_id =>'113')
Author.create(:presentation_id => '33', :user_id =>'114')
Author.create(:presentation_id => '33', :user_id =>'115')
Author.create(:presentation_id => '33', :user_id =>'116')
Author.create(:presentation_id => '34', :user_id =>'117')
Author.create(:presentation_id => '34', :user_id =>'118')
Author.create(:presentation_id => '34', :user_id =>'119')
Author.create(:presentation_id => '35', :user_id =>'120')
Author.create(:presentation_id => '35', :user_id =>'121')
Author.create(:presentation_id => '35', :user_id =>'122')
Author.create(:presentation_id => '35', :user_id =>'123')
Author.create(:presentation_id => '36', :user_id =>'124')
Author.create(:presentation_id => '36', :user_id =>'125')
Author.create(:presentation_id => '36', :user_id =>'126')
Author.create(:presentation_id => '36', :user_id =>'127')
Author.create(:presentation_id => '36', :user_id =>'128')
Author.create(:presentation_id => '36', :user_id =>'129')
Author.create(:presentation_id => '36', :user_id =>'130')
Author.create(:presentation_id => '37', :user_id =>'131')
Author.create(:presentation_id => '37', :user_id =>'132')
Author.create(:presentation_id => '38', :user_id =>'133')
Author.create(:presentation_id => '38', :user_id =>'134')
Author.create(:presentation_id => '38', :user_id =>'135')
Author.create(:presentation_id => '38', :user_id =>'136')
Author.create(:presentation_id => '38', :user_id =>'137')
Author.create(:presentation_id => '38', :user_id =>'138')
Author.create(:presentation_id => '39', :user_id =>'137')
Author.create(:presentation_id => '39', :user_id =>'138')
Author.create(:presentation_id => '39', :user_id =>'139')
Author.create(:presentation_id => '39', :user_id =>'140')
Author.create(:presentation_id => '40', :user_id =>'141')
Author.create(:presentation_id => '40', :user_id =>'142')
Author.create(:presentation_id => '40', :user_id =>'143')
Author.create(:presentation_id => '40', :user_id =>'144')
Author.create(:presentation_id => '41', :user_id =>'145')
Author.create(:presentation_id => '41', :user_id =>'146')
Author.create(:presentation_id => '41', :user_id =>'147')
Author.create(:presentation_id => '41', :user_id =>'148')
Author.create(:presentation_id => '42', :user_id =>'149')
Author.create(:presentation_id => '42', :user_id =>'150')
Author.create(:presentation_id => '42', :user_id =>'151')
Author.create(:presentation_id => '42', :user_id =>'152')
Author.create(:presentation_id => '42', :user_id =>'153')
Author.create(:presentation_id => '42', :user_id =>'144')

####################################################################################################

