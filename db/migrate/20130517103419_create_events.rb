class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :conference_id
      t.string :title
      t.string :local
      t.string :notes
      t.datetime :start_at
      t.datetime :end_at

      t.timestamps
    end
  end
end
