json.attendee_id @attendee.id

json.schedule_id @schedule.id

json.sessions @schedule.sessions do |session|
     json.partial! 'sessions/session', session: session
end

json.events @schedule.events do |event|
     json.partial! 'events/event', event: event
end