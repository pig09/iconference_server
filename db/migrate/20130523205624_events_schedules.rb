class EventsSchedules < ActiveRecord::Migration
  def change
    create_table 'events_schedules', :id => false do |t|
      t.integer :event_id
      t.integer :schedule_id
    end
    add_index(:events_schedules, [:event_id, :schedule_id])

  end
end
