class AddHashedPasswordToOrgMember < ActiveRecord::Migration
  def change
    add_column :org_members, :hashed_password, :string
  end
end
