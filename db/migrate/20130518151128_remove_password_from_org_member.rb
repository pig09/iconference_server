class RemovePasswordFromOrgMember < ActiveRecord::Migration
  def up
    remove_column :org_members, :password
  end

  def down
    add_column :org_members, :password, :string
  end
end
