class Presentation < ActiveRecord::Base
  attr_accessible :article, :end_at, :session_id, :start_at, :title

  #Associations
  belongs_to :session
  has_many :authors
  has_many :users, :through => :authors
  has_many :speakers
  has_many :users, :through => :speakers

  #Validadtions
  validates_presence_of :session

  validates :title, :presence => true, :uniqueness => true
  #validates :article, :presence => true #, :uniqueness => true
  validates :start_at, :presence => true
  validates :end_at, :presence => true

  def duration
    dur = (end_at - start_at) / 60
    dur.to_s
  end

  def room_name
    room_id = Session.find(session_id).room_id
    Room.find(room_id).name
  end

  def room_map
    room_id = Session.find(session_id).room_id
    Room.find(room_id).map_url
  end

  def speaker
    if self.speakers.exists?
      self.speakers.first.user.name
    end

  end

  def chair
    self.session.chair
  end

end
