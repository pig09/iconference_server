module Admin
  class AccessController < ApplicationController
      def new
        render 'access/new'
      end

      def create
        member = OrgMember.find_by_email(params[:access][:email].downcase)
        if member && OrgMember.authenticate(member.email, params[:access][:password])
          sign_in member
          flash.now[:success] = 'Welcome back!'
          redirect_to '/admin/dashboard'
        else
          flash.now[:error] = 'Invalid email/password combination'
          render 'access/new'
        end
      end

      def destroy
        sign_out
        redirect_to root_url
      end
  end
end

