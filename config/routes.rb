Iconference::Application.routes.draw do
  
  resources :maps


  root :to => 'static_pages#home'
  
  # Static pages
  match '/home',    to: 'static_pages#home',    via: 'get'
  match '/help',    to: 'static_pages#help',    via: 'get'
  match '/about',    to: 'static_pages#about',    via: 'get'
  match '/contact',    to: 'static_pages#contact',    via: 'get'



  ##########################
  #
  #       API/V1
  #
  ##########################
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do

      resources :conferences do
        resources :sessions, :presentations, :authors, :speakers, :events, :maps
        resources :sessions do
          :presentations
        end
      end

      match '/authenticate', to: 'access#create'
    end
  end


  
  ##########################
  #
  #       ADMIN
  #
  ##########################

  namespace :admin, defaults: {format: 'html'} do
    match '/dashboard', :to => 'dashboard#menu', via: 'get'
    resources :access, only: [:new, :create, :destroy]
    resources :institutions, :org_members, :attendees
    match '/signin',  to: 'access#new'
    match '/signout', to: 'access#destroy', via: 'delete'

    resources :conferences do
      resources :sessions, :authors, :speakers, :events,
                :attendees, :maps
      resources :sessions do
        :presentations
      end
      member do
        post 'import'
      end
    end
  end

  ##########################
  #
  #       DEVELOPMENT
  #
  ##########################

  resources :users, :attendees, :authors, :events, :coffee_breaks,
  :events, :institutions, :keynotes,
  :org_members, :presentations, :rooms,
  :sessions, :speakers, :welcome_sessions, :workshops, :maps


  resources :schedules do
    collection do
      put 'add'
      delete 'remove'
    end
  end


  resources :conferences do
    get 'program', :on => :member
    resources :events, :sessions, :presentations, :authors, :speakers, :maps
    end

  resources :presentations do
    resources :authors, :speakers
  end


  resources :sessions do
    resources :presentations
  end

=begin
  resources :schedules do
    resources :sessions do
      get 'delete', :on => :collection
    end
  end
=end
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.

  match ':controller/:id/:action'
  match ':controller/:id/:action/:presentation_id'
  match ':controller(/:action(/:id))(.:format)'

end
