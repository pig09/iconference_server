require 'test_helper'

class CoffeeBreaksControllerTest < ActionController::TestCase
  setup do
    @coffee_break = coffee_breaks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:coffee_breaks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create coffee_break" do
    assert_difference('CoffeeBreak.count') do
      post :create, coffee_break: { notes: @coffee_break.notes }
    end

    assert_redirected_to coffee_break_path(assigns(:coffee_break))
  end

  test "should show coffee_break" do
    get :show, id: @coffee_break
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @coffee_break
    assert_response :success
  end

  test "should update coffee_break" do
    put :update, id: @coffee_break, coffee_break: { notes: @coffee_break.notes }
    assert_redirected_to coffee_break_path(assigns(:coffee_break))
  end

  test "should destroy coffee_break" do
    assert_difference('CoffeeBreak.count', -1) do
      delete :destroy, id: @coffee_break
    end

    assert_redirected_to coffee_breaks_path
  end
end
