class CreateAttendees < ActiveRecord::Migration
  def change
    create_table :attendees do |t|
      t.integer :user_id
      t.integer :conference_id
      t.integer :schedule_id
      t.boolean :checkin
      t.text :log
      t.text :favorites

      t.timestamps
    end
  end
end
