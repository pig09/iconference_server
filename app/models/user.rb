class User < ActiveRecord::Base
  attr_accessible :biografy, :birthDate, :device, :email , :email_confirmation, :facebook, :firstName, :institution_id, :lastName, :linkedin, :photo, :preferences, :telephone, :twitter, :password, :password_confirmation
  
  serialize :preferences,Array
  
  has_secure_password

  # ASSOCIATIONS
  has_one :institution
  has_many :attendees
  has_many :conferences, :through =>:attendees
  has_many :authors
  has_many :presentations, :through =>:authors
  has_many :speakers
  has_many :presentations, :through=>:speakers
  has_many :api_keys

  before_save {|user| user.email = email.downcase}
  before_save
    
  # VALIDATIONS
  EMAIL_REGEX = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i

  #validates :birthDate, :presence => true
  #validates :device, :presence => true
  #validates :photo, :presence => true
  #validates :telephone, :presence => true
  validates :firstName, :presence => true, :length => {:minimum => 2 }
  validates :lastName, :presence => true, :length => {:minimum => 2 }
  validates :email, :presence => true, :uniqueness => { case_sensitive: false }#, :confirmation => true
  # :format => EMAIL_REGEX
  #validates :password, presence: true, length: { :minimum => 6 }
  #validates :password_confirmation, presence: true

  public
  
  def name
    "#{firstName} #{lastName}"
  end       

  def affiliated?
    !institution_id.blank?
  end

  def affiliation
    Institution.find(institution_id).name
  end
end
