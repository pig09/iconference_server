module Api
  module V1
    class UsersController < ApplicationController
      include RestrictAccess
      
      respond_to :json
      
      # GET /api/v1/users
      def index
        respond_with User.all
      end
      
      # GET /api/v1/users/:id
      def show
        respond_with User.find(params[:id])
      end 
      
      # POST /api/v1/us
      def create
        user = User.create(params[:product])
        device = APN::Device.new
        device.token = self.format_apn_token(params[:device_token])
        respond_with
      end
      
      def update
        respond_with User.update(params[:id], params[:product])
      end
      
      def destroy
        respond_with User.destroy(params[:id])
      end
      
      
      private
      
      #def restrict_access
      #  api_key = ApiKey.find_by_access_token(params[:access_token])
      #  head :unauthorized unless api_key
      #end
      def restrict_access
        authenticate_or_request_with_http_token do |token, options|
          ApiKey.exists?(access_token: token)
        end
      end
    end

    def format_apn_token(text)
      text && text.gsub(/(.{8})(?=.)/, '\1 \2')
    end

  end  
end