class Room < ActiveRecord::Base
  attr_accessible :capacity, :map, :name , :remote_map_url

  #Associations
  has_many :sessions

  mount_uploader :map, RoomMapUploader

  #Validations
  #validates :name, :presence => true, :uniqueness => true
  #validates :capacity, :presence => true, :numericality => { :only_integer => true, :greater_than => 0  }

  def map_url
    if Rails.env == 'development' && map != nil
      "http://localhost:3000#{map}"
    elsif Rails.env == 'production' && map != nil
      "http://193.136.122.140#{map}"
    else
      map
    end
  end

end
