class WelcomeSessionsController < ApplicationController
  # GET /welcome_sessions
  # GET /welcome_sessions.json
  def index
    @welcome_sessions = WelcomeSession.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @welcome_sessions }
    end
  end

  # GET /welcome_sessions/1
  # GET /welcome_sessions/1.json
  def show
    @welcome_session = WelcomeSession.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @welcome_session }
    end
  end

  # GET /welcome_sessions/new
  # GET /welcome_sessions/new.json
  def new
    @welcome_session = WelcomeSession.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @welcome_session }
    end
  end

  # GET /welcome_sessions/1/edit
  def edit
    @welcome_session = WelcomeSession.find(params[:id])
  end

  # POST /welcome_sessions
  # POST /welcome_sessions.json
  def create
    @welcome_session = WelcomeSession.new(params[:welcome_session])

    respond_to do |format|
      if @welcome_session.save
        format.html { redirect_to @welcome_session, notice: 'Welcome session was successfully created.' }
        format.json { render json: @welcome_session, status: :created, location: @welcome_session }
      else
        format.html { render action: "new" }
        format.json { render json: @welcome_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /welcome_sessions/1
  # PUT /welcome_sessions/1.json
  def update
    @welcome_session = WelcomeSession.find(params[:id])

    respond_to do |format|
      if @welcome_session.update_attributes(params[:welcome_session])
        format.html { redirect_to @welcome_session, notice: 'Welcome session was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @welcome_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /welcome_sessions/1
  # DELETE /welcome_sessions/1.json
  def destroy
    @welcome_session = WelcomeSession.find(params[:id])
    @welcome_session.destroy

    respond_to do |format|
      format.html { redirect_to welcome_sessions_url }
      format.json { head :no_content }
    end
  end
end
