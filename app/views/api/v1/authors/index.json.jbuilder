json.authors @authors do |author|
	json.partial! 'authors/author', author: author, user: author.user
end