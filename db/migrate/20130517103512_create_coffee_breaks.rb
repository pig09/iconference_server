class CreateCoffeeBreaks < ActiveRecord::Migration
  def change
    create_table :coffee_breaks do |t|
      t.string :notes

      t.timestamps
    end
  end
end
