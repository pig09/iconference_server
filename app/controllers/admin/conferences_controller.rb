module Admin
  class ConferencesController < ApplicationController

    def show
      @conference = Conference.find(params[:id])
      #render 'conferences/show'
    end

    # GET /conferences/1/edit
    def edit
      @conference = Conference.find(params[:id])
    end

    # PUT /conferences/1
    def update
      @conference = Conference.find(params[:id])
      if @conference.update_attributes(params[:conference])
        redirect_to @conference
        flash[:success] = 'Conference was successfully updated.'
      else
        render action: "edit"
      end
    end

    # GET /conferences/1/program
    def program
      @sessions = Session.find_all_by_conference_id(params[:id])
      @events = Conference.find(params[:id]).events
      respond_to do |format|
        format.html
        format.json { render 'conferences/program.json.jbuilder' }
      end
    end

    # GET /conferences/1/import
    def import
      begin
      @conference = Conference.find(params[:id])

      f = params[:file].open
      doc = Nokogiri::XML(f)
      root = doc.root

      #Info
      info = root.xpath('info')
      self.parse_info(info)

      sessions = root.xpath('sessions/session')
      print "\n"+'Number of sessions: '+sessions.count.to_s+"\n"

      @conference.sessions.delete_all
      sessions.each do |session|
        print "\nSession:\n"+session+"\n"
        self.parse_session(session)
      end

       @conference.save
        redirect_to admin_conference_url(@conference)
        flash[:success] = 'Conference imported successfully'


      rescue
        redirect_to admin_conference_url(@conference)
        flash[:error] = 'Unable to import conference'
      end
    end

    def parse_info(info)
      @conference.name = info.at_xpath('//name').text
      @conference.local = info.at_xpath('//local').text
      @conference.description = info.at_xpath('//description').text
      @conference.conf_date = Date.strptime(info.at_xpath('//date').text, '%d/%m/%Y')
    end

    def parse_session(session)
      new_session = Session.new
      print  "\n\n\n" + session.at_xpath('//name').text
      new_session.name = session.at_xpath('//name').text
      new_session.chair = session.at_xpath('//chair').text

      new_room = Room.new
      new_room.name = session.at_xpath('//room/name').text
      new_room.capacity = Integer(session.at_xpath('//room/capacity').text)
      new_room.save

      new_session.room_id = new_room.id

      new_session.conference_id = @conference.id

      new_session.save

      print 'session_id: '+new_session.id.to_s
      print 'session_conference:' + new_session.conference_id.to_s

      presentations = session.xpath('presentations/presentation')
      print "\n"+'Number of presentations: '+presentations.count.to_s+"\n"

      presentations.each do |presentation|
        print presentation
        self.parse_presentation(new_session, presentation)
      end
    end

    def parse_presentation(session, presentation)
      new_presentation = Presentation.new
      print "\n"+'Presentation title: '+presentation.at_xpath('//title').text+"\n"

      new_presentation.title = presentation.at_xpath('//title').text
      print "\n"+'Presentation start_at: '+DateTime.strptime(presentation.at_xpath('//start').text, '%d/%m/%Y %H:%M').to_s+"\n"
      new_presentation.start_at = DateTime.strptime(presentation.at_xpath('//start').text, '%d/%m/%Y %H:%M')
      new_presentation.end_at = DateTime.strptime(presentation.at_xpath('//end').text, '%d/%m/%Y %H:%M')
      new_presentation.article = presentation.at_xpath('//article').text

      # authors

      new_presentation.session_id = session.id
      new_presentation.save
    end



  end
end

