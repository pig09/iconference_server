require 'socket'

class Conference < ActiveRecord::Base


  attr_accessible :avatar, :conf_date, :description, :local, :name, :hashtag, :remote_avatar_url


  has_many :attendees
  has_many :users, :through => :attendees
  has_many :sessions
  has_many :events
  has_and_belongs_to_many :org_members, :join_table => 'org_members_conferences'
  has_and_belongs_to_many :maps, :join_table => 'conferences_maps'


  mount_uploader :avatar, ConferenceAvatarUploader

  validates :local, :presence => true
  #if the picture will be stored in "assets", there's no need to verify format of URL.. GIF etc
  validates :conf_date, :presence => true
  validates :description, :presence => true
  validate :word_count_is_less_than_100
  validates :name, :presence => true , :uniqueness => true



  public

  def start_at
    conf_date
  end

  def avatar_url
    if Rails.env == 'development' && avatar != nil
      "http://localhost:3000#{avatar}"
    elsif Rails.env == 'production' && avatar != nil
        "http://193.136.122.140#{avatar}"
    else
      avatar
    end
  end

  def end_at
      dates = []
      self.sessions.each do |session|
        dates.append session.start_at.to_date
      end
      self.events.each do |event|
        dates.append event.end_at.to_date
      end
      dates.sort!.last


  end

  def import(file)

    f = File.open(file)
    doc = Nokogiri::XML(f)
    print doc
    parse_xml(doc)
  end

  private

  def word_count_is_less_than_100
    errors[:conference_description] << 'Too many words' if description.split.size > 99
  end

  def parse_xml(doc)
    doc.root.elements.each do |node|
      if node.node_name.eql? 'info'
        parse_info(node)
      end
    end
  end

  def parse_info(node)
    node.elements.each do |node|
      self.name = node.text.to_s if node.name.eql? 'name'
    end
  end

  def local_ip
    orig, Socket.do_not_reverse_lookup = Socket.do_not_reverse_lookup, true  # turn off reverse DNS resolution temporarily

    UDPSocket.open do |s|
      s.connect '64.233.187.99', 1
      s.addr.last
    end
  ensure
    Socket.do_not_reverse_lookup = orig
  end

end
