class AddSaltToOrgMember < ActiveRecord::Migration
  def change
    add_column :org_members, :salt, :string
  end
end
