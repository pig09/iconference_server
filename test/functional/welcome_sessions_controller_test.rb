require 'test_helper'

class WelcomeSessionsControllerTest < ActionController::TestCase
  setup do
    @welcome_session = welcome_sessions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:welcome_sessions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create welcome_session" do
    assert_difference('WelcomeSession.count') do
      post :create, welcome_session: { speaker_id: @welcome_session.speaker_id }
    end

    assert_redirected_to welcome_session_path(assigns(:welcome_session))
  end

  test "should show welcome_session" do
    get :show, id: @welcome_session
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @welcome_session
    assert_response :success
  end

  test "should update welcome_session" do
    put :update, id: @welcome_session, welcome_session: { speaker_id: @welcome_session.speaker_id }
    assert_redirected_to welcome_session_path(assigns(:welcome_session))
  end

  test "should destroy welcome_session" do
    assert_difference('WelcomeSession.count', -1) do
      delete :destroy, id: @welcome_session
    end

    assert_redirected_to welcome_sessions_path
  end
end
