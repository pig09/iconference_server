class CreateOrgMembers < ActiveRecord::Migration
  def change
    create_table :org_members do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :password

      t.timestamps
    end
  end
end
