class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.integer :room_id
      t.integer :conference_id
      t.string :name
      t.string :chair

      t.timestamps
    end
  end
end
