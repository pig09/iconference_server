class Keynote < Event
  attr_accessible :speaker_id

  validates :speaker_id, :presence => true
end
