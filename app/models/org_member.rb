require 'digest/sha1'

class OrgMember < ActiveRecord::Base

  # ASSOCIATIONS
  has_and_belongs_to_many :conferences, :join_table => 'org_members_conferences'

  # ATTRIBUTES
  attr_accessible :email, :first_name, :last_name # mass_assignment available
  attr_protected :hashed_password, :salt # cannot be mass_assigned
  attr_accessor :password  # non db

  #has_secure_password

  # VALIDATIONS
  EMAIL_REGEX = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
  validates :email, :presence => true, :length => { :maximum => 100 } #, :format => EMAIL_REGEX, :confirmation => true
  validates :first_name, :presence => true
  validates :last_name, :presence => true
  #validates :password, :presence => true #, :confirmation => true
  #validates :password_confirmation, :presence => true

  # only on create, so other  attributes of this user can be changed
  validates_length_of :password, :within => 4..20, :on => :create

  before_save :create_hashed_password
  after_save :clear_password

  #####################################################################

  public

  def name
    "#{first_name} #{last_name}"
  end


  def self.authenticate(email='', password='')

     member = OrgMember.find_by_email(email)
    if member && member.password_match?(password)
      return member
    else
      return false
    end
  end

  def password_match?(password='')
     hashed_password == OrgMember.hash_with_salt(password, salt)
  end

  def self.make_salt(last_name='')
    Digest::SHA1.hexdigest("Use #{last_name} with #{Time.now} to make salt.")
  end

  def self.hash_with_salt(password='', salt='')
    Digest::SHA1.hexdigest("Put #{salt} on the #{password}.")
  end



  #####################################################################

  private

  def create_hashed_password
    unless password.blank?
      self.salt = OrgMember.make_salt(last_name) if salt.blank?
      self.hashed_password = OrgMember.hash_with_salt(password, salt)
    end
  end

  def clear_password
    self.password = nil
  end

end