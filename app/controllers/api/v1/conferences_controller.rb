module Api
  module V1
    class ConferencesController < ApplicationController
      include RestrictAccess
      
      respond_to :json
      
      # GET /api/v1/conferences
      def index
        @conferences = Conference.all
        render :file => 'conferences/index.json.jbuilder'
      end
      
      # GET /api/v1/conferences/:id
      def show
        @conference = Conference.find(params[:id])
        render :file => 'conferences/show.json.jbuilder'
      end 
      
      # GET /api/v1/conferences/new
      def new
        respond_with Conference.create(params[:conference])
      end
      
      #edit
      
      #POST api/v1/conferences
      def create
        respond_with Conference.create(params[:conference])
      end
      
      # PUT api/v1/conferences/1
      def update
        respond_with Conference.update(params[:id], params[:conference])
      end
      
      # DELETE api/v1/conferences/1
      def destroy
        respond_with Conference.destroy(params[:id])
      end
      
    end
  end  
end