module Admin
  class SessionsController < ApplicationController

    def index
      @conference = Conference.find(params[:conference_id])
      @sessions = @conference.sessions
    end


    def show
      @conference = Conference.find(params[:conference_id])
      @session = Session.find(params[:id])
    end


    def new
      @conference = Conference.find(params[:conference_id])
      @session = Session.new
    end



    def edit
      @conference = Conference.find(params[:conference_id])
      @session = Session.find(params[:id])
    end


    def create
      @conference = Conference.find(params[:conference_id])
      @session = Session.new(params[:session])
      if @session.save
        redirect_to @session, notice: 'Session was successfully created.'
      else
        render action: 'new'
      end
    end



    # PUT /sessions/1
    # PUT /sessions/1.json
    def update
      @session = Session.find(params[:id])

      respond_to do |format|
        if @session.update_attributes(params[:session])
          format.html { redirect_to @session, notice: 'Session was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @session.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /sessions/1
    # DELETE /sessions/1.json
    def destroy
      @conference = Conference.find(params[:conference_id])
      @session = Session.find(params[:id])
      if@session.check_for_presentations
        @pres = Presentation.find_all_by_session_id(params[:id])

        @pres.each do |presentation|
          presentation.destroy
        end
      end
      @session.destroy
      redirect_to admin_conference_sessions_url(@conference)

    end
  end
end