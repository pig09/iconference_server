require 'test_helper'

class OrgMembersControllerTest < ActionController::TestCase
  setup do
    @org_member = org_members(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:org_members)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create org_member" do
    assert_difference('OrgMember.count') do
      post :create, org_member: { email: @org_member.email, first_name: @org_member.first_name, last_name: @org_member.last_name, password: @org_member.password }
    end

    assert_redirected_to org_member_path(assigns(:org_member))
  end

  test "should show org_member" do
    get :show, id: @org_member
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @org_member
    assert_response :success
  end

  test "should update org_member" do
    put :update, id: @org_member, org_member: { email: @org_member.email, first_name: @org_member.first_name, last_name: @org_member.last_name, password: @org_member.password }
    assert_redirected_to org_member_path(assigns(:org_member))
  end

  test "should destroy org_member" do
    assert_difference('OrgMember.count', -1) do
      delete :destroy, id: @org_member
    end

    assert_redirected_to org_members_path
  end
end
