require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
    @user = users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post :create, user: { biografy: @user.biografy, birthDate: @user.birthDate, device: @user.device, email: @user.email, facebook: @user.facebook, firstName: @user.firstName, institution_id: @user.institution_id, lastName: @user.lastName, linkedin: @user.linkedin, password: @user.password, photo: @user.photo, preferences: @user.preferences, telephone: @user.telephone, twitter: @user.twitter }
    end

    assert_redirected_to user_path(assigns(:user))
  end

  test "should show user" do
    get :show, id: @user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user
    assert_response :success
  end

  test "should update user" do
    put :update, id: @user, user: { biografy: @user.biografy, birthDate: @user.birthDate, device: @user.device, email: @user.email, facebook: @user.facebook, firstName: @user.firstName, institution_id: @user.institution_id, lastName: @user.lastName, linkedin: @user.linkedin, password: @user.password, photo: @user.photo, preferences: @user.preferences, telephone: @user.telephone, twitter: @user.twitter }
    assert_redirected_to user_path(assigns(:user))
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete :destroy, id: @user
    end

    assert_redirected_to users_path
  end
end
