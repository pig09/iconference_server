class Schedule < ActiveRecord::Base
  # attr_accessible :title, :body
  
  #Associations
  has_one :attendee
  has_and_belongs_to_many :sessions, :join_table => 'sessions_schedules'
  has_and_belongs_to_many :events
  
end
