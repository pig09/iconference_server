class AddValidToApiKey < ActiveRecord::Migration
  def change
    add_column :api_keys, :expired, :boolean
  end
end
