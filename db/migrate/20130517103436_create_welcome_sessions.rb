class CreateWelcomeSessions < ActiveRecord::Migration
  def change
    create_table :welcome_sessions do |t|
      t.integer :speaker_id

      t.timestamps
    end
  end
end
