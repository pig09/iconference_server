module Api
  module V1
    class AccessController < ApplicationController
      #http_basic_authenticate_with name: 'admin', password: 'secret'
      #  def create
      #    @user = authenticate_with_http_basic { |u, p| User.authenticate(u, p) }
      #   if @user
      #      @apikey = ApiKey.create!
      #      @apikey.user_id = @user.id
      #      @apikey.valid = true
      #      render :file => '/users/show.json.jbuilder'
      #   else
      #     head :unauthorized
      #   end
      # end
      def create
        @user = authenticate_with_http_basic { |u, p|
          user = User.find_by_email(u)
          if user
          user.authenticate(p)
          else
            head :unauthorized
          end
        }
        
        if @user and @user != ' '
          @user.api_keys.create!
          #session[:user_id] = @authenticated_user.id
          #redirect_to lists_url, notice: "Logged in!"
          render  :file => 'users/show.json.jbuilder'
        else
          head :unauthorized
        end
      end
      
      def destroy
        #sign_out
        #redirect_to root_url
      end  
    end
  end
end
