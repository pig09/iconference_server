class ConferencesController < ApplicationController
 
  #respond_with :html
 
  # GET /conferences
  def index
    @conferences = Conference.all
    # index.html.erb
  end

  # GET /conferences/1
  def show
    @conference = Conference.find(params[:id])
    # show.html.erb
  end

  # GET /conferences/new
  def new
    @conference = Conference.new
    # new.html.erb
  end

  # GET /conferences/1/edit
  def edit
    @conference = Conference.find(params[:id])
  end

  # POST /conferences
  def create
    @conference = Conference.new(params[:conference])
    if @conference.save
      redirect_to @conference
      flash[:success] = 'Conference was successfully created.'
    else
      render action: "new"
      flash[:error] = 'An error occoured. Please try again later.'
    end
  end

  # PUT /conferences/1
  def update
    @conference = Conference.find(params[:id])
    if @conference.update_attributes(params[:conference])
      redirect_to @conference 
      flash[:success] = 'Conference was successfully updated.'
    else
      render action: "edit" 
    end
  end

  # DELETE /conferences/1
  def destroy
    @conference = Conference.find(params[:id])
    @conference.destroy
    redirect_to conferences_url
  end

  # GET /conferences/1/program
  def program
    @sessions = Session.find_all_by_conference_id(params[:id])
    @events = Conference.find(params[:id]).events
    respond_to do |format|
      format.html
      format.json { render 'conferences/program.json.jbuilder' }
    end
  end
end


