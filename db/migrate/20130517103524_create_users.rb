class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :firstName
      t.string :lastName
      t.integer :telephone
      t.date :birthDate
      t.integer :institution_id
      t.string :photo
      t.string :password
      t.string :email
      t.string :apn_device_id

      t.timestamps
    end
  end
end
