=begin
task :deliver_notifications => [:environment] do
  APN::App.first.send_notifications
end
=end

namespace :apn do
  task :init => [:environment] do # NOTE: One-time task...

    print "Defining environment....\n"
    APN::App::ENV = Rails.env

    print "Creating APN::App...\n"
    app = APN::App.new


    print "Adding development certificate...\n"
    app.apn_dev_cert = Rails.root.join('config', 'apple_push_notification_development.pem').read
    app.apn_prod_cert = Rails.root.join('config', 'apple_push_notification_production.pem').read
    app.save!

    print "Registering testing device...\n"
    device = APN::Device.new
    #device.token = '27db4bf8 5e5cb5ad 9c1e0498 48fe6247 f309f0bc 356a96a4 0d2e8a6c da3a4666'
    device.token = '6b00a1d3 c9ab3024 7c9198a1 a2ba41b5 46a6255c 7d74764b 818084c9 5c1ccba0'
    device.app_id=app.id
    device.save

    puts ((app.valid? && device.valid?) ? 'Done' : 'Failed')
  end
end


