# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130630005248) do

  create_table "api_keys", :force => true do |t|
    t.string   "access_token"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "user_id"
    t.boolean  "expired"
  end

  create_table "apn_apps", :force => true do |t|
    t.text     "apn_dev_cert"
    t.text     "apn_prod_cert"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "apn_device_groupings", :force => true do |t|
    t.integer "group_id"
    t.integer "device_id"
  end

  add_index "apn_device_groupings", ["device_id"], :name => "index_apn_device_groupings_on_device_id"
  add_index "apn_device_groupings", ["group_id", "device_id"], :name => "index_apn_device_groupings_on_group_id_and_device_id"
  add_index "apn_device_groupings", ["group_id"], :name => "index_apn_device_groupings_on_group_id"

  create_table "apn_devices", :force => true do |t|
    t.string   "token",              :default => "", :null => false
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.datetime "last_registered_at"
    t.integer  "app_id"
  end

  add_index "apn_devices", ["token"], :name => "index_apn_devices_on_token"

  create_table "apn_group_notifications", :force => true do |t|
    t.integer  "group_id",          :null => false
    t.string   "device_language"
    t.string   "sound"
    t.string   "alert"
    t.integer  "badge"
    t.text     "custom_properties"
    t.datetime "sent_at"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "apn_group_notifications", ["group_id"], :name => "index_apn_group_notifications_on_group_id"

  create_table "apn_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "app_id"
  end

  create_table "apn_notifications", :force => true do |t|
    t.integer  "device_id",                        :null => false
    t.integer  "errors_nb",         :default => 0
    t.string   "device_language"
    t.string   "sound"
    t.string   "alert"
    t.integer  "badge"
    t.datetime "sent_at"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.text     "custom_properties"
  end

  add_index "apn_notifications", ["device_id"], :name => "index_apn_notifications_on_device_id"

  create_table "apn_pull_notifications", :force => true do |t|
    t.integer  "app_id"
    t.string   "title"
    t.string   "content"
    t.string   "link"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.boolean  "launch_notification"
  end

  create_table "attendees", :force => true do |t|
    t.integer  "user_id"
    t.integer  "conference_id"
    t.integer  "schedule_id"
    t.boolean  "checkin"
    t.text     "log"
    t.text     "favorites"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "authors", :force => true do |t|
    t.integer  "user_id"
    t.integer  "presentation_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "coffee_breaks", :force => true do |t|
    t.string   "notes"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "conferences", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "local"
    t.string   "avatar"
    t.string   "hashtag"
    t.date     "conf_date"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "conferences_maps", :id => false, :force => true do |t|
    t.integer "conference_id"
    t.integer "map_id"
  end

  add_index "conferences_maps", ["conference_id", "map_id"], :name => "index_conferences_maps_on_conference_id_and_map_id"

  create_table "events", :force => true do |t|
    t.integer  "conference_id"
    t.string   "title"
    t.string   "local"
    t.string   "notes"
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "speaker_id"
    t.string   "type"
  end

  create_table "events_schedules", :id => false, :force => true do |t|
    t.integer "event_id"
    t.integer "schedule_id"
  end

  add_index "events_schedules", ["event_id", "schedule_id"], :name => "index_events_schedules_on_event_id_and_schedule_id"

  create_table "institutions", :force => true do |t|
    t.string   "name"
    t.string   "country"
    t.string   "website"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "keynotes", :force => true do |t|
    t.integer  "speaker_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "maps", :force => true do |t|
    t.string   "name"
    t.string   "map"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "org_members", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "hashed_password"
    t.string   "salt"
  end

  create_table "org_members_conferences", :id => false, :force => true do |t|
    t.integer "org_member_id"
    t.integer "conference_id"
  end

  add_index "org_members_conferences", ["org_member_id", "conference_id"], :name => "index_org_members_conferences_on_org_member_id_and_conference_id"

  create_table "presentations", :force => true do |t|
    t.integer  "session_id"
    t.string   "title"
    t.string   "article"
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "rooms", :force => true do |t|
    t.string   "name"
    t.integer  "capacity"
    t.string   "map"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "schedules", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "sessions", :force => true do |t|
    t.integer  "room_id"
    t.integer  "conference_id"
    t.string   "name"
    t.string   "chair"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "sessions_schedules", :id => false, :force => true do |t|
    t.integer "session_id"
    t.integer "schedule_id"
  end

  add_index "sessions_schedules", ["session_id", "schedule_id"], :name => "index_sessions_schedules_on_session_id_and_schedule_id"

  create_table "speakers", :force => true do |t|
    t.integer  "user_id"
    t.integer  "presentation_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "firstName"
    t.string   "lastName"
    t.integer  "telephone"
    t.date     "birthDate"
    t.integer  "institution_id"
    t.string   "photo"
    t.string   "password"
    t.string   "email"
    t.string   "apn_device_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "password_digest"
  end

  create_table "welcome_sessions", :force => true do |t|
    t.integer  "speaker_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "workshops", :force => true do |t|
    t.integer  "speaker_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
