class AuthorsController < ApplicationController


  # GET /authors
  # GET /authors.json
  def index
    if( params[:presentation_id] )
      @authors = Presentation.find(params[:presentation_id]).authors

    elsif ( params[:conference_id])
      sessions = Conference.find(params[:conference_id]).sessions
      @authors = []
      sessions.each do |session|
        presentations = session.presentations
        presentations.each do |presentation|
          @authors += presentation.authors
        end
      end
=begin
      # http://consortium-pi.herokuapp.com/services/info/json?author=ana&filiation=fct



      uri = URI.parse("http://google.com/")

      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)

      response = http.request(request)

      response.code             # => 301
      response.body             # => The body (HTML, XML, blob, whatever)
                                # Headers are lowercased
      response["cache-control"] # => public, max-age=2592000
=end
    else
      @authors = Author.all

      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @authors }
      end

    end
  end

  # GET /authors/1
  # GET /authors/1.json
  def show
    @author = Author.find(params[:id])
    @user = @author.user


    respond_to do |format|
      format.html # show.html.erb
      format.json { render 'authors/show' }
    end
  end

  # GET /authors/new
  # GET /authors/new.json
  def new
    @author = Author.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @author }
    end
  end

  # GET /authors/1/edit
  def edit
    @author = Author.find(params[:id])
  end

  # POST /authors
  # POST /authors.json
  def create
    @author = Author.new(params[:author])

    respond_to do |format|
      if @author.save
        format.html { redirect_to @author, notice: 'Author was successfully created.' }
        format.json { render json: @author, status: :created, location: @author }
      else
        format.html { render action: "new" }
        format.json { render json: @author.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /authors/1
  # PUT /authors/1.json
  def update
    @author = Author.find(params[:id])

    respond_to do |format|
      if @author.update_attributes(params[:author])
        format.html { redirect_to @author, notice: 'Author was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @author.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /authors/1
  # DELETE /authors/1.json
  def destroy
    @author = Author.find(params[:id])
    @author.destroy

    respond_to do |format|
      format.html { redirect_to authors_url }
      format.json { head :no_content }
    end
  end
end
