class Session < ActiveRecord::Base
  attr_accessible :chair, :conference_id, :name, :room_id

  has_one :room
  has_many :presentations
  belongs_to :conference
  has_and_belongs_to_many :schedules, :join_table => 'sessions_schedules'

  #validations

  #validates_presence_of :conference
  #validates_presence_of :room

  validates :chair, :presence => true
  validates :name, :presence => true #, :uniqueness => true


  before_destroy :check_for_presentations

  public

  def check_for_presentations
    if Presentation.find_all_by_session_id(self.id).count > 0
      return true
    end
  end

  def sorted_presentations
      presentations = Presentation.find_all_by_session_id(self.id)
      presentations.sort_by{|e| e[:start_at]}
  end

  def start_at
    if presentations.count > 0
      self.sorted_presentations.first.start_at
    end

  end

  def end_at
    if presentations.count > 0
      self.sorted_presentations.last.end_at
    end
  end

  def room
    Room.find(self.room_id).name
  end
end
