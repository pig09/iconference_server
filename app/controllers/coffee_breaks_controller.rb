class CoffeeBreaksController < ApplicationController
  # GET /coffee_breaks
  # GET /coffee_breaks.json
  def index
    @coffee_breaks = CoffeeBreak.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @coffee_breaks }
    end
  end

  # GET /coffee_breaks/1
  # GET /coffee_breaks/1.json
  def show
    @coffee_break = CoffeeBreak.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @coffee_break }
    end
  end

  # GET /coffee_breaks/new
  # GET /coffee_breaks/new.json
  def new
    @coffee_break = CoffeeBreak.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @coffee_break }
    end
  end

  # GET /coffee_breaks/1/edit
  def edit
    @coffee_break = CoffeeBreak.find(params[:id])
  end

  # POST /coffee_breaks
  # POST /coffee_breaks.json
  def create
    @coffee_break = CoffeeBreak.new(params[:coffee_break])

    respond_to do |format|
      if @coffee_break.save
        format.html { redirect_to @coffee_break, notice: 'Coffee break was successfully created.' }
        format.json { render json: @coffee_break, status: :created, location: @coffee_break }
      else
        format.html { render action: "new" }
        format.json { render json: @coffee_break.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /coffee_breaks/1
  # PUT /coffee_breaks/1.json
  def update
    @coffee_break = CoffeeBreak.find(params[:id])

    respond_to do |format|
      if @coffee_break.update_attributes(params[:coffee_break])
        format.html { redirect_to @coffee_break, notice: 'Coffee break was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @coffee_break.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coffee_breaks/1
  # DELETE /coffee_breaks/1.json
  def destroy
    @coffee_break = CoffeeBreak.find(params[:id])
    @coffee_break.destroy

    respond_to do |format|
      format.html { redirect_to coffee_breaks_url }
      format.json { head :no_content }
    end
  end
end
