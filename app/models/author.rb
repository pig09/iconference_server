class Author < ActiveRecord::Base
  attr_accessible :presentation_id, :user_id
  attr_accessor :homepage
  
  #Associations
  belongs_to :user
  belongs_to :presentation

  #Validations

  validates_presence_of :user
  validates_presence_of :presentation

  #before_save :retrieve_homepage



  private

  def retrieve_homepage
    response = RestClient.get 'http://consortium-pi.herokuapp.com/services/info/json', {:params => {'author' => user.name , 'filiation' => Institution.find(user.institution_id).name }}
    if response.code  == 200
      json = JSON.parse(response)
      self.homepage = URI::decode(json['authorHomepage'])
      print self.homepage+"\n"
    end

  end
end
