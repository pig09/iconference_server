class Speaker < ActiveRecord::Base
  attr_accessible :presentation_id, :user_id
  
  #Associations
  belongs_to :user
  belongs_to :presentation

  #Validations
  validates_presence_of :user
  validates_presence_of :presentation
  
end
