json.author_id author.id
json.user_id user.id
json.name user.name
json.photo user.photo
json.email user.email
json.institution user.affiliation
json.homepage author.homepage
