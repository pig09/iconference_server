json.results @events do |event|
   json.partial! 'events/event', event: event
end