require 'nokogiri'

namespace :conference do

  task :import do
    print 'imported'
  end
end


namespace :nokogiri do

  task :xml do
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.root {
        xml.products {
          xml.widget {
            xml.id_ "10"
            xml.name "Awesome widget"
          }
        }
      }
    end
    puts builder.to_xml
  end

  task :parse do

  end


end