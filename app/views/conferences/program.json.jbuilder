json.sessions @sessions do |session|
    json.partial! 'sessions/session', session: session
end

json.events @events do |event|
    json.partial! 'events/event', event: event
end

