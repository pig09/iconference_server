module Admin
  class DashboardController < ApplicationController
    def index
      menu
      render 'menu'
    end

    def menu
      @member =  current_user
      render 'menu'
    end
  end
end
