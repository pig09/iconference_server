
    json.count @sessions.count
    json.sessions @sessions do |session|
        json.(session, :id, :name, :start_at, :end_at, :created_at, :updated_at)
    end
