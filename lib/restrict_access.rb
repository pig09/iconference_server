module RestrictAccess
  protected
  
  def self.included(base)
    base.before_filter :before_filter_grant_access
  end
  
  # To authenticate with URL token uncomment next def
  # /../users?access_token="..."
  
 
  def before_filter_grant_access
    api_key = ApiKey.find_by_access_token(params[:access_token])
    head :unauthorized unless api_key
  end


  # To authenticate with HTML HEADER token uncomment next def
  # /../users?access_token="..."

=begin
  def before_filter_grant_access
    authenticate_or_request_with_http_token do |token, options|
      ApiKey.exists?(access_token: token)
    end
  end
=end
end
 