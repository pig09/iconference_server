	json.id session.id
    json.name session.name
    json.type 'session'
    json.local session.room
    json.(session, :id, :start_at, :end_at)