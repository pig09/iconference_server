class CreateOrgMembersConferences < ActiveRecord::Migration
  def change
    create_table 'org_members_conferences', :id => false do |t|
      t.integer :org_member_id
      t.integer :conference_id
    end
    add_index(:org_members_conferences, [:org_member_id, :conference_id])
  end
end
