json.results @presentations do |presentation|
   json.partial! 'presentations/presentation', presentation: presentation
end