class SessionsSchedules < ActiveRecord::Migration
  def change
    create_table 'sessions_schedules', :id => false do |t|
      t.integer :session_id
      t.integer :schedule_id
    end
    add_index(:sessions_schedules, [:session_id, :schedule_id])
  end

end
