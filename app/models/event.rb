class Event < ActiveRecord::Base
  attr_accessible :conference_id, :end_at, :local, :notes, :start_at, :title

  belongs_to :conference
  belongs_to :speaker
  has_and_belongs_to_many :schedules

  #Validations
  #validates :local, :presence => true
  #validates :end_at, :presence => true
  #validates :start_at, :presence => true
  #validates :title, :presence => true, :uniqueness => true

  def duration
    dur = (end_at - start_at) / 60
    dur.to_s
  end

end
