class CreateConferences < ActiveRecord::Migration
  def change
    create_table :conferences do |t|
      t.string :name
      t.text :description
      t.string :local
      t.string :avatar
      t.string :hashtag
      t.date :conf_date

      t.timestamps
    end
  end
end
