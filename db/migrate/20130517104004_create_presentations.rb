class CreatePresentations < ActiveRecord::Migration
  def change
    create_table :presentations do |t|
      t.integer :session_id
      t.string :title
      t.string :article
      t.datetime :start_at
      t.datetime :end_at

      t.timestamps
    end
  end
end
