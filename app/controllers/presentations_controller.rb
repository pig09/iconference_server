class PresentationsController < ApplicationController

  #include(APN)
  require 'apn_on_rails'

  # GET /presentations
  # GET /presentations.json
  def index
    if (params[:session_id] != nil)
      @presentations = Presentation.where(:session_id => params[:session_id])
      render 'presentations/index'
    elsif (params[:schedule_id] != nil)
      @presentations = Schedule.find(params[:schedule_id]).presentations
    elsif (params[:conference_id])
      sessions = Conference.find(params[:conference_id]).sessions
      @presentations = []
      sessions.each do |session|
        @presentations += session.presentations
      end

      render 'presentations/index'
    else
      @presentations = Presentation.all

      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @presentations }
      end
    end


  end


  # GET /presentations/1
  # GET /presentations/1.json
  def show
    @presentation = Presentation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render 'presentations/show.json.jbuilder' }
    end
  end


  # GET /presentations/new
  # GET /presentations/new.json
  def new
    @presentation = Presentation.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @presentation }
    end
  end


  # GET /presentations/1/edit
  def edit
    @presentation = Presentation.find(params[:id])
  end


  # POST /presentations
  # POST /presentations.json
  def create
    if( params[:session_id] != nil)

    elsif (params[:schedule_id] != nil)
      schedule = Schedule.find(params[:schedule_id])
      presentations = params[:presentations]
      presentations.each do |presentation|
        schedule.presentations << Presentation.find(presentation[:id])
      end
      render :nothing => true
    else
      @presentation = Presentation.new(params[:presentation])
      respond_to do |format|
        if @presentation.save
          format.html { redirect_to @presentation, notice: 'Presentation was successfully created.' }
          format.json { render json: @presentation, status: :created, location: @presentation }
        else
          format.html { render action: "new" }
          format.json { render json: @presentation.errors, status: :unprocessable_entity }
        end
      end
    end
  end


  # PUT /presentations/1
  # PUT /presentations/1.json
  def update
    @presentation = Presentation.find(params[:id])
    respond_to do |format|

      if @presentation.update_attributes(params[:presentation])


        device = APN::Device.first

        notification = APN::Notification.new
        notification.device = device
        notification.alert = format('Presentation %{name} updated.', :name => @presentation.title )
        #notification.alert = 'Presentation updated.'
        notification.badge = 1
        notification.sound = true
        #notification.custom_properties = {:id => @presentation.id}
        notification.save

        APN::App.first.send_notifications

        #group = APN::Group.new
        #group.name = 'test'
        #group.app_id = APN::App.first
        #group.save

        #device = APN::Device.first

        #app = APN::App.first
        #APN::App.send_notifications

        #devices = []
        #devices + APN::Device.first

        #group.devices = devices
        #APN::App.send_group_notifications
        #APN::App.process_devices


          redirect_to @presentation
          flash[:success] = 'Presentation was successfully updated.'
      else
        format.html { render action: "edit" }
        format.json { render json: @presentation.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /presentations/1
  # DELETE /presentations/1.json
  def destroy
    if (params[:session_id] != nil)

    elsif (params[:schedule_id] != nil)
      schedule = Schedule.find(params[:schedule_id])
      schedule.presentations.delete(Presentation.find(params[:id]))
      render :nothing => true
    else
      @presentation = Presentation.find(params[:id])
      @presentation.destroy

      respond_to do |format|
        format.html { redirect_to presentations_url }
        format.json { head :no_content }
      end
    end
  end


  def delete
    schedule = Schedule.find(params[:schedule_id])
    presentations = params[:presentations]
    presentations.each do |presentation|
      schedule.presentations.delete(Presentation.find(presentation[:id]))
    end
    render :nothing => true
  end




  ######################################################

  def attendeePres
      #@attendee = Attendee.find(params[:id])

      #allConf=Array.new

        @cenas= Schedule.find(1).presentations.all

        #allConf.push(Schedule.find(@attendee.schedule_id).presentations.all)


      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @cenas }
      end
    end

  def authors
    @authors = Author.find_all_by_presentation_id(params[:id])
    authorsArr=Array.new
    @authors.each do |user|
        authorsArr.push(User.find_all_by_id(user.user_id))
     end


    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: authorsArr }
    end
  end

  def speaker
    @speakers = Speaker.find_all_by_presentation_id(params[:id])
    speakersArr=Array.new
    @speakers.each do |user|
      speakersArr.push(User.find_all_by_id(user.user_id))
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: speakersArr }
    end
  end



end
