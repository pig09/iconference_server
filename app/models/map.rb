class Map < ActiveRecord::Base
  attr_accessible :name, :map  , :remote_map_url

  has_and_belongs_to_many :conferences, :join_table => 'conferences_maps'

  mount_uploader :map, MapUploader
end
