class CreateWorkshops < ActiveRecord::Migration
  def change
    create_table :workshops do |t|
      t.integer :speaker_id

      t.timestamps
    end
  end
end
