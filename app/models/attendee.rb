class Attendee < ActiveRecord::Base
  attr_accessible :checkin, :conference_id, :favorites, :log, :schedule_id, :user_id

  serialize :favorites,Array
  serialize :log,Array

  #Associations

  belongs_to :user
  belongs_to :conference
  has_many :rates
  has_many :requests
  has_many :presentations, :through => :rates
  has_many :notifications
  belongs_to :schedule

  # VALIDATIONS
  #validates_presence_of :conference

  validates :checkin, :presence => true#, :inclusion => { :in => [true, false],:message => "%{value} is not a valid checkin" }


  after_create :check_for_existing_users

  #verifica caso quando se crie um attendee, não se especifique o user_id visto não existir
  #é criado um utilizador e passado o seu ID para o user_id do Attendee
  def check_for_existing_users
    if self.user_id.blank?
      Attendee.find(self.id).update_attribute(:user_id,User.count+1)
      User.create()
    end
  end
end
