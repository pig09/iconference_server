class CreateKeynotes < ActiveRecord::Migration
  def change
    create_table :keynotes do |t|
      t.integer :speaker_id

      t.timestamps
    end
  end
end
