  json.id event.id
  json.name event.title
  json.type 'event'
  json.local event.local
  json.(event, :id, :start_at, :end_at, :duration)