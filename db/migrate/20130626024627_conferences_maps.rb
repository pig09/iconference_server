class ConferencesMaps < ActiveRecord::Migration
  def change
    create_table 'conferences_maps', :id => false do |t|
      t.integer :conference_id
      t.integer :map_id
    end
    add_index(:conferences_maps, [:conference_id, :map_id])

  end
end
