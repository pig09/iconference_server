module AccessHelper


  def sign_in(member)
      session[:member_id] = member.id
      self.current_user = member
    end

    def signed_in?
      !current_user.nil?
    end

    def sign_out
      self.current_user = nil
      session[:member_id] = nil
    end

    def current_user=(user)
        @current_user = user
    end

    def current_user
      @current_user ||= OrgMember.find_by_id(session[:member_id])
    end

end
